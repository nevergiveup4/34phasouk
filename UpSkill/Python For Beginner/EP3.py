# ການນຳໃຊ້ print
# string => ຂໍ້ຄວາມຕົວອັກສອນ ' "" '''
# number => integer , float
# boolean => true , false
print(type(-1))
print(0)
print(1)
print(type(3.99))
print(type(True))
print(False)
print("Phasouk \n" + "Louangchalern")  