# Type Conversion ຄືການແປງຊະນິດຂໍ້ມູນ
 
x = 10
y = 3.14
z = "OPS"

# ບວກເລກ 
result = x + y # ສູດການບວກເລກໃນພາສາພາຍທອນ 
print (result)


####

scores = {'james': 1828, 'thomas': 3628, 'danny': 9310}
scores['bobby'] = 4401

numbers = {1: 'One', 2: 'Two', 3: 'Three'}

print(scores)
print(numbers)

scores = {'james': 1828, 'thomas': 3628, 'danny': 9310, 'bobby': 4401}

# display data
print('james =>', scores['james'])
print('thomas =>', scores['thomas'])
print('danny =>', scores['danny'])
print('bobby =>', scores['bobby'])

# update data
scores['james'] = scores['james'] + 1000
scores['thomas'] = 100

print('james =>', scores['james'])
print('thomas =>', scores['thomas'])


countries = {'de': 'Germany', 'ua': 'Ukraine',
             'th': 'Thailand', 'nl': 'Netherlands'}

print(countries.keys())
print(countries.values())

print(countries.get('de')) # equal to countries['de']
countries.setdefault('tr', 'Turkey')

print(countries.popitem())
print(countries.popitem())

print(countries.items())