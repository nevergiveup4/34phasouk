## Using Indentation in python for false
## ນິຍາມງ່າຍໆເລີຍຄື Indentation in Python ຫຼັກການເຮັດວຽກຄື ພາຍໃນບ໊ອກຍ່ອຍຈະຕ້ອງມີຫຍໍ້ໜ້າທຸກຄັ້ງຈື່ງສາມາດລັນໄດ້ຫຼືຖືກຕ້ອງຕາມໂປແກຮມ
'''
ນີ້ແມ່ນລັກສະນະການນຳໃຊ້ Indentation in python ທີ່ຜິດເພາະໃນພາສາໄພຖອນເລື່ອງການມີຫຍໍ້ໜ້າຂອງບ໊ອກ
ໃນຄຳສັ່ງເປັນເລື່ອງທີ່ສຳຄັນຫຼາຍ ຖ້າບໍ່ມີຫຍໍ້ໜ້າສະແດງວ່າໂປແປຮມນັ້ນລັນບໍ່ໄດ້

if 5 > 2:
print('Five is greater than two')

'''

'''
ຕົວຢ່າງທີ່ນຳໃຊ້ indentation in python ທີ່ຖືກຕ້ອງ

if 5 > 2:

    print('five is greater than two')

'''

n = int(input ('Input an integer: '))

if(n > 0):
    print('x is positive number')
    print('Show number from 0 to %d' % (n-1))

else:
    print ('x isn\'t positive number')

for i in range(n):
    print(i)