## Bitwise Operators is operators Level Bit of data or manage information in system arithmethic 2 layer 

'''
Bitwise Operators Have to :

1. Operators: &  // name Bitwise and // Result a & b
2. Operators: |  // name Bitwise or // Result a | b
3. Operators: ^  // name Bitwise Exclusive or (XOR) // Result a ^ b
4. Operators: <<  // name Bitwise shifted left // Result a << b
5. Operators: >>  // name Bitwise shifted right // Result a >> b
6. Operators: ~  // name Bitwise invert // Result ~a

'''

## Example using to:


a = 3 
b = 5


print('a & b =', a & b)
print('a | b =', a | b)
print('a ^ b =', a ^ b)
print('~a =', ~a)
print('a << 1 =', a << 1)
print('a << 2 =', a << 2)
print('100 >> 1 =', 100 >> 1)