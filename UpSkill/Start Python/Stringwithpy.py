
name = "Mateo"
site = 'marcuscode.com'
str1 = "This is my string"
str2 = 'This is my string'


sentent1 = "What's your name?"
sentent2 = 'I\'m Phasouk.'
sentent3 = "He said \"I would learn Python first\"."
sentent4 = 'His teach replied "Oh well!"'
print(sentent1)
print(sentent2)
print(sentent3)
print(sentent4)
raw_str = r"Python\tJava\tPHP\n"

print(raw_str)

str = """\
HTTP response code
     200               Success
     404               Not found
     503               Service unavailable 
"""

print(str)


first_name = 'Phasouk'
last_name = 'LOUANGCHALERN'
full_name = first_name +' '+ last_name
bless = 'Happy' + ' NewYear China'

print(full_name)
print(bless)

# \ ຕໍ່່ທ້າຍປະໂຫຍດຈະເປັນການບໍ່ມີລົງແຖວໃໝ່ໂຕທີ່ຂຽນດ້ານລຸ່ມຈະຂຽນຕໍ່ກັນໄປດັ່ງໂຄດລຸ່ມນີ້
name = 'Matt' 'Williams'
my_number = 'One '\
'Two '\
'Three '

print(name)
print(my_number)