'''

การสืบทอดคลาส (Inheritance) คือการที่เราสามารถสร้างคลาสใหม่มาจากคลาสเดิมได้ 
และเพิ่มเติมความสามารถบางอย่างเข้าไป นี่เป็นการนำโค้ดเดิมกลับมาใช้ใหม่และลดการเขียนโค้ดเดิมซ้ำๆ 
ยกตัวอย่างเช่น หากเรามีคลาสของทรงกลมอยู่แล้ว เราสามารถสร้างคลาสของลูกบอลโดยสืบทอดมาจากคลาสทรงกลมได้
นั่นเป็นเพราะว่าลูกบอลก็เป็นทรงกลมอยู่แล้ว ดังนั้นสิ่งที่ต้องเพิ่มก็คือคุณสมบัติบางอย่างที่ลูกบอลไม่มี
ต่อไปเราจะสร้างคลาสของกล่องที่มีสีสันโดยสืบทอดมาจากคลาส Box ในตัวอย่างก่อนหน้า นี่เป็นตัวอย่างของโปรแกรม


'''

class Box:
    def __init__(self, width , height):
        self.width = width
        self.height = height

    def getArea(self):
        return self.width * self.height
    
class ColorfulBox(Box):
    def __init__(self, width ,height, color):
        super().__init__(width, height)
        self.color = color

    def draw(self):
        print("Drawing the %s box" % self.color)
        for i in range(1 , self.height + 1):
            print("%s" % ("#" * self.width))

# Creating colorful box

box = ColorfulBox(4, 3, "Red")
print("Box has width %d and height %d" %(box.width, box.height))

print("Is's area is %d " %box.getArea())
box.draw()



