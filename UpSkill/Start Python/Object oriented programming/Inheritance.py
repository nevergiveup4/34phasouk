'''

Inheritance หรือการสืบทอด คือการที่คลาสหรือออบเจ็ค ได้รับการถ่ายทอดแอตทริบิวต์และเมธอดจากคลาสอื่น 
นั่นจะทำให้คลาสดังกล่าวมีแอตทริบิวต์และเมธอดเหมือนคลาสที่มันสืบทอดมาเรียกคลาสนั้นว่า super class หรือ base class 
ส่วนคลาสที่ได้รับการสืบทอดเรียกว่า sub class หรือ child class นอกจากนี้ เรายังสามารถขยายความสามารถโดยการเพิ่มแอตทริบิวต์หรือเมธอด 
หรือทำการ override เมธอดของ super class ได้ นี่เป็นแนวคิดในการนำโค้ดเดิมกลับมาใช้โดยไม่ต้องเขียนขึ้นใหม่ และเพิ่มความสามารถของเดิมที่มีอยู่
ในภาษา Python นั้นยังสนับสนุน Multiple inheritance ซึ่งอนุญาติให้คลาสสืบทอดจากคลาสหลายๆ คลาสได้ในเวลาเดียวกัน

นี่เป็นรูปแบบของการสืบทอดคลาสในภาษา Python

'''

'''

class DerivedClassName(BaseClassName):
    pass

class DerivedClassName(BaseClassName1, BaseClassName2, ...):
    pass

'''

# Example inheritance :

class Person:
    def __init__(self, firstName, lastName):
        self.firstName = firstName
        self.lastName = lastName

    def getName(self):
        return self.firstName + ' ' + self.lastName
    
class Employee(Person):

    def setWorkDetail(self, department , position):
        self.department = department
        self.position = position 

    def getWorkDetail(self):
        return self.position + ', ' + self.department
        
emp1 = Employee('Phasouk', 'LOUANGCHALERN')
emp1.setWorkDetail('Computer Enginner' , 'Python and Golang Programmer')
print('Name: ' + emp1.getName())
print('Work: ' + emp1.getWorkDetail())

emp2 =Employee('Elon', 'Musk')
emp2.setWorkDetail('Tesla' , 'Technology')

print('Name: ' + emp2.getName())
print('Work: ' + emp2.getWorkDetail())


        
