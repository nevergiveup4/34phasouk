'''

เป็นการสืบทอดคลาสจากเพียงแค่คลาสเดียว อย่างไรก็ตามในภาษา Python
นั้นสนับสนุนการสืบทอดจากหลายคลาสได้ในพร้อมกัน ต่อไปมาดูตัวอย่างของโปรแกรมที่จะใช้ประโยชน์จากการสืบทอดหลายคลาส

'''
class Geographic:

    def setCordinate(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

    def getCordinate(self):
        return str(self.latitude) + ', ' + str(self.longitude)

    def getTimeZone(self):
        timezone = round(self.longitude / 12 - 1)
        if timezone > 0:
            return '+' + str(timezone)
        else:
            return str(timezone)

    def getClimate(self):
        if self.latitude <= -66.5 or self.latitude >= 66.5:
            return 'Polar zone'
        elif self.latitude <= -23.5 or self.latitude >= 23.5:
            return 'Temperate zone'
        else:
            return 'Tropical zone'   

class Temperature:

    def setCelsius(self, celsius):
        self.celsius = celsius

    def getFahrenheit(self):
        return self.celsius * 1.8 + 32

    def getKelvin(self):
        return self.celsius + 273.15

    def getWeather(self):
        if self.celsius <= 0:
            return 'freezing'
        elif self.celsius <= 18:
            return 'cold'
        elif self.celsius <= 28:
            return 'warm'
        else:
            return 'hot'

class Country(Geographic, Temperature):

    def __init__(self, name, area, population):
        self.name = name
        self.area = area
        self.population = population

    def getPopulationDensity(self):
       return self.population / self.area

    def showDetails(self):
        print('Country: %s' % self.name)
        print('Area: %.2f sq km' % self.area)
        print('Population: %d' % self.population)
        print('Density: %.2f person per sq km' % 
        self.getPopulationDensity())
        print('Decimal cordinate: %s' % self.getCordinate())
        print('Time zone: %s' % self.getTimeZone())
        print('Climate: %s' % self.getClimate())
        print('Temperature in Celsius: %.2f degree' % self.celsius)
        print('Temperature in Fahrenheit: %.2f degree' % 
        self.getFahrenheit())
        print('Temperature in Kelvin: %.2f' % self.getKelvin())
        print('The weather is %s' % self.getWeather())
        print()

c = Country('Thailand', 513120, 68863514)
c.setCordinate(13.75 , 100.483333)
c.setCelsius(28.5)
c.showDetails()

c2 = Country('England', 130279, 55268100)
c2.setCordinate(51.5 , -0.116667)
c2.setCelsius(9)
c2.showDetails()

c2 = Country('Canada', 9984670, 35151728)
c2.setCordinate(45.5, -75.666667)
c2.setCelsius(-3)
c2.showDetails()