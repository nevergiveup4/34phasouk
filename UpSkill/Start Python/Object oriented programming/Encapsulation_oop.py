class Television:
    __volume = 20

    def __init__(self, model):
        self.model = model

    def volumeUp(self):
        self.__volume = self.__volume + 1
        if self.__volume > 30:
            self.__volume = 30
        print("Increased volume up to %d" % self.__volume)

    def volumeDown(self):
        self.__volume = self.__volume - 1
        if self.__volume < 0:
            self.__volume = 0
        print("Decreased volume down to %d" % self.__volume)

    def getVolume(self):
        return self.__volume

    def info(self):
        print("TV Model: %s" % self.model)
        print("Current volume: %s%%" % self.__volume)

tv = Television("LG")

# using our TV
print("%s TV has volume level at %d%%." % (tv.model, tv.getVolume()))

tv.volumeUp()
tv.volumeUp()
tv.info()