def calBoxArea(width , height):
    return width * height

box1_width = 3
box1_height = 4

box2_width = 5
box2_height = 3

print("Area of box1 is %d" %calBoxArea(box1_width , box1_height))
print("Area of box2 is %d" %calBoxArea(box2_width, box2_height))


'''

นี่เป็นการเขียนโปรแกรมแสดงการทำงานของกล่องเช่นเดิม แต่ไม่ได้ใช้แนวคิดของการเขียนโปรแกรมเชิงวัตถุแล้ว ดังนั้นข้อมูลของกล่องไม่ว่าจะเป็น ความกว้าง ความยาว และฟังก์ชันคำนวณหาพื้นที่จะถูกประกาศแยกออกจากกัน จะเห็นว่าการเขียนโปรแกรมในรูปแบบนี้มีความเรียบง่าย แต่เมื่อระบบใหญ่และซับซ้อนขึ้นมันก็ทำให้การจัดการยากขึ้นเช่นเดียวกัน ดังนั้นการเขียนโปรแกรมเชิงวัตถุจะช่วยแก้ปัญหานี้ได้โดยรวมโค้ดที่เกี่ยวข้องกันให้เป็นหนึ่งเดียว

'''