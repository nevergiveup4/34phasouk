'''

__init__() คือ เมธอดพิเศษเอาไว้ใช้สำหรับเริ่มต้น (Initialize) คลาส

__init__( ) และ self ใน Python
ในภาษา Python นั้น   __init__()  เป็นเมธอดพิเศษ (Special or Dunder Method) หรือที่เรียกว่าคอนสตรัคเตอร์ (Constructor) ก็ได้เช่นกัน ซึ่ง  __init__()  จะถูกเรียกใช้โดยอัตโนมัติเมื่อมีการสร้างออบเจกต์ของคลาสขึ้นมา หรือเรียกได้ว่าจะเป็นส่วนของการเริ่มต้น (initialize) ของ class


เมธอด  __init__()ใช้พารามิเตอร์   self  ซึ่งเป็นการอ้างอิงถึงวัตถุที่กำลังสร้าง เพื่อให้สามารถเข้าถึงคุณสมบัติ (Attributes) และเมธอด (Methods) ของวัตถุ โดยพารามิเตอร์  self  ต้องเป็นพารามิเตอร์แรกในเมธอดที่กำหนดในคลาส ถึงแม้ว่าใน Python เราสามารถกำหนดพารามิเตอร์เป็นชื่ออะไรก็ได้ที่ไม่ใช่ self แต่สุดท้ายแล้วก็คือธรรมเนียมปฏิบัติ (convension) ของภาษาว่าควรต้องใช้  self ถึงจะเหมาะสมที่สุด

เมธอด (method) คือฟังก์ชันที่อยู่ภายในคลาส โดยเมธอดจะเป็นตัวกำหนดการกระทำ (behavior) ของคลาส ถ้าสังเกตให้ดีจะเห็นว่า method ก็คือฟังก์ชัน ๆ หนึ่ง เพียงแต่ว่าเป็นฟังก์ชันที่อยู่ภายใต้คลาสนั่นเอง



'''

class Box:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def getArea(self):
        return self.width * self.height

box1 = Box(width = 10 , height = 10)
box2 = Box(5, 3)

print("Area of box1 is %d" % box1.getArea())
print("Area of box2 is %d" % box2.getArea())


