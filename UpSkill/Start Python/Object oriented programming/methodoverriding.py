'''

ຮຽນຮູ້ກ່ຽວກັບການ override method ເຊິ່ງຄືການທີ່ sub class ທຳການກຳນົດການທຳງານຂອງ Methods
ຈາກ new super class ໂດຍຍັງຄົງໃຊ້ຊື່ເດີມເຊິ່ງຈະເຮັດຄລາດໃຫ່ມເກີດຂື້ນໃນບໍລິບົດຂອງ sub class ແລະ methods ຈາກ super class ຈະບໍ່ສາມາດ
ເຂົ້າເຖິງມັນໄດ້ ຕົວຢ່າງ :

'''

class animal:
    def move(self):
        print('Annimal is moving')

class Dog(animal):

    def move(self):
        print('Dog is running')

    def parentMove(self):
        print('Call parent')
        animal.move(self)

a = animal()
a.move()

d = Dog()
d.move()
d.parentMove()

'''

ในตัวอย่าง เราได้สร้างคลาส Animal โดยคลาสนี้มีเมธอด move() สำหรับแสดงข้อความการเคลื่อนที่ของสัตว์ ต่อมาเราได้สร้างคลาส Dog ซึ่งเป็นคลาสที่สืบทอดมาจากคลาส Animal ในคลาส Dog เราได้ทำการเขียนการทำงานของเมธอด move() ใหม่ เพื่อบอกว่าการเคลื่อนที่ของสุนัขนั้นคือการวิ่ง ดังนั้นการทำงานของเมธอดในคลาสหลักจึงถูกทับไป

อย่างไรก็ตาม เรายังคงสามารถเรียกใช้งานเมธอดจากคลาสหลักได้ ในเมธอด parentMove() เป็นการเรียกใช้งานเมธอดจาก super class ในคำสั่ง Animal.move(self) ในตอนนี้ถึงแม้ว่าเราจะได้ทำการ override เมธอดนี้ไปแล้ว แต่เราก็ยังสามารถเรียกใช้มันได้เช่นเดิม

'''