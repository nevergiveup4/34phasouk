# calculate time passed from date to now
if time_dif < 60:
    print(math.floor(time_dif), 'secs ago')
elif time_dif <= 3600:
   print(math.floor(time_dif / 60), 'mins ago')
elif time_dif <= 86400:
   print(math.floor(time_dif / 3600), 'hours ago')
elif time_dif <= 86400 * 30:
   print(math.floor(time_dif / 86400), 'days ago')
elif time_dif <= 86400 * 365.25:
   print(math.floor(time_dif / (86400 * 30)), 'months ago')
else:
   print(math.floor(time_dif / (86400 * 365.25)), 'years ago')