'''
if, if else and elif 
    ເປັນຄຳສັ່ງຄວບຄຸມທີ່ເຮັດວຽກຕາມເງື່ອນໄຂໃດເງື່ອນໄຂໜື່ງຄາມລຳດັບ 
        ເມືອມີການຖືກຕາມເງື່ອນໄຂໃດໜື່ງບໍ່ວ່າຈະເປັນ 1,2,3,4 ລົງໄປຕົກທີ່ເງື່ອນໄຂໃດກ່ອນກໍຈະທຳງານຢູ່ເງື່ອນໄຂນັ້ນ 
            ແລ້ວຈະອອກຈາກລູບໄປໂດຍອັດຕະໂນມັດ

'''

# Example using to : if expression: statements 

n = 10
if n == 10:
    print('n equal to 10')

    logged_in = False
    if not logged_in:
        print(' You nust login to continue ')

        m = 4 
        if m % 2 == 0 and m > 0:
            print('m us even and positive numbers')

            if 3 > 10:
                print('This Block isn\'t executed')

                ## example using to if else :

                n = 5
                if n == 10:
                   print('n equal to 10')
                else:
                    print('Who are you?')

                    money = 300
                    if money >= 350:
                        print('You can buy an Ipad')
                    else:
                        print('You don\'t have enough money to buy an Ipad')

                        
