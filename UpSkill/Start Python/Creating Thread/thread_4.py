'''

Thread นั้นจะทำงานพร้อมกันแบบคู่ขนาน รวมทั้ง Thread หลักด้วย เพื่อแสดงให้คุณได้เห็น มาดูตัวอย่างของ Thread ที่มีการทำงานนานกว่าในตัวอย่างที่เราได้เห็นก่อนหน้า เราจะเขียนโปรแกรมเพื่อแสดงตัวเลข 1 - 5 จากภายใน Thread

'''

from threading import Thread

class Counter (Thread):

    def __init__(self, end):
        Thread.__init__(self)
        self.end = end

    def run(self):
        for i in range(1, self.end + 1):
            print(self.name + " : " + str(i))


thr1 = Counter(3)
thr2 = Counter(6)

thr1.start()

thr2.start()