from threading import Thread 

class BubbleSorting(Thread):

    def __init__(self, numbers):
        Thread.__init__(self)
        self.numbers = numbers

    def run(self):
        n = len(self.numbers)
        for i in range(0, n):
            for j in range(0, n - i - 1):
                if self.numbers[j] > self.numbers[j+1]:
                    temp = self.numbers[j]
                    self.numbers[j] = self.numbers[j + 1]
                    self.numbers[j + 1] = temp

# List of numbers to sort
numbers = [8, 14, 4, 2, 1, 17, 12, 3, 0, 4, 16, 11]

# Slice list into half and supply to each thread
thr1 = BubbleSorting(numbers[0:6])
thr2 = BubbleSorting(numbers[6:12])

thr1.start()
thr2.start()

# Wait for all threads to complete
thr1.join()
thr2.join()

# Obtain sorted lists from threads
list1 = thr1.numbers
list2 = thr2.numbers

len1 = len(list1)
len2 = len(list2)

# Merge sorted lists to final list
sorted_numbers = []
i = j = 0
while i < len1 and j < len2:
    if list1[i] <= list2[j]:
        sorted_numbers.append(list1[i])
        i += 1
    else:
        sorted_numbers.append(list2[j])
        j += 1

while (i < len1):
    sorted_numbers.append(list1[i])
    i += 1

while (j < len2):
    sorted_numbers.append(list1[j])
    j += 1

print("Sorted from Thread-1:")
print(list1)

print("Sorted from Thread-2:")
print(list2)

print("Final sorted:")
print(sorted_numbers)

