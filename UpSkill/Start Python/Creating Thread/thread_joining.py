'''

เมธอด join() นั้นเป็นเมธอดที่รอให้ Thread ทำงานให้จบก่อนที่จะทำในคำสั่งต่อไป 
เราใช้เมธอดนี้ในกรณีที่ต้องการรอการทำงานบางอย่างจาก Thread ยกตัวอย่างเช่น 
ก่อนที่จะทำงาน Thread ที่สองอาจจะต้องรอผลลัพธ์การทำงานจาก Thread แรกก่อน 
ดังนั้นก่อนที่เรียกใช้งาน Thread ที่สอง เราจำเป็นต้องรอให้ Thread แรกทำงานเสร็จก่อน มาดูตัวอย่าง

'''

from threading import Thread

class Counter(Thread):

    def __init__(self, end):
        Thread.__init__(self)
        self.end = end

    def run(self):
        for i in range(1, self.end + 1):

            print(self.name + " : " + str(i))


thr1 = Counter(5)
thr1.start()


# Block until thread 1 is done 
thr1.join()


thr2 = Counter(5)
thr2.start()