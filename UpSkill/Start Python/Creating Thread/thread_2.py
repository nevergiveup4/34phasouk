import threading

def thread_callback(name, loop):
    for i in range(1, loop + 1):
        print("%sL %i" %(name, i))

thr = threading.Thread(target= thread_callback, args = ["Thread-a", 5])
thr.start()

