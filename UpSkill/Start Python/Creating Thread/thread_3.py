'''

เราได้สร้าง Thread ออบเจ็คจากคลาส Thread ซึ่งเป็นคลาสไลบรารี่ของภาษา Python ที่ใช้สำหรับสร้าง Thread ที่ไม่ซับซ้อนมาก อย่างไรก็ตาม คุณสามารถสร้างคลาส Thread ของคุณได้โดยการสืบทอดคลาสของคุณจากคลาส Thread นี่เป็นตัวอย่าง

'''

from threading import Thread

class MyThread(Thread):

    def __init__(self, firstName):
        Thread.__init__(self)
        self.firstName = firstName

    def run(self):
        print("Hello " + self.firstName + " from " + self.name)

thr1 = MyThread("Phasouk")
thr2 = MyThread("LOUANGCHALERN")
thr3 = MyThread("Python Programmer")

thr1.start()
thr2.start()
thr3.start()