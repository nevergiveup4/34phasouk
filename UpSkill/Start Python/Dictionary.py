'''

Dictionary คือประเภทข้อมูลที่เก็บข้อมูลในรูปแบบคู่ของ Key และ Value โดยที่ Key ใช้สำหรับเป็น Index ในการเข้าถึงข้อมูลและ Value เป็นค่าข้อมูลที่สอดคล้องกับ Key ของมัน 
การเข้าถึงข้อมูลใน Dictionary นั้นรวดเร็วเพราะว่าข้อมูลได้ถูกทำ Index ไว้อัตโนมัติโดยใช้ Key นอกจากนี้ Dictionary ยังมีเมธอดและฟังก์ชันอำนวยความสะดวกสำหรับการทำงานทั่วไป

'''

scores = {'james': 1828, 'thomas': 3628, 'danny': 9310}
scores['bobby'] = 4401

numbers = {1: 'One', 2: 'Two', 3: 'Three'}

print(scores)
print(numbers)


# Access infor in Dictionary
#Display Data

print('james =>' , scores['james'])
print('thomas =>', scores['thomas'])
print('danny =>', scores['danny'])
print('bobby =>', scores['bobby'])
print('1: ', numbers[1])

# Update Data ຄືການເພີ່ມຈຳນວນເຂົ້າໄປຕື່ມ 
'''

Dictionary == Name_variable = Key : Value
ในการเข้าถึงข้อมูลภายใน Dictionary นั้น
คุณต้องตรวจสอบให้แน่ใจว่า Key นั้นมีอยู่จริง ไม่เช่นนั้นโปรแกรมจะเกิดข้อผิดพลาดขึ้น

'''

scores['james'] = scores['james'] + 1000
scores['thomas'] = 100

print('james =>', scores['james'])
print('thomas =>', scores['thomas'])





