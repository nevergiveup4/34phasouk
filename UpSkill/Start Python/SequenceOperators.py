'''
ໃນພາສາ Python ມີຕົວດຳເນີນການໃນການກວດສອບການເປັນສະມາຊິກໃນອັອບເຈັກປະເພດ List Tuple and Dictionary 
ຕົວດຳເນີນການ in ໃຊ້ໃນການກວດສອບ ຖ້າຫາກຄ່ານັ້ນມີຢູ່ໃນອັອບເຈັກ ຖ້າຫາກພົບເຫັນຈະໄດ້ຜົນລັບເປັນ True  ແລະຫາກວ່າ ບໍ່ພົບເຫັນຕາມເງື່ອນໄຂຊື່ຕົວແປທີ່ຕັ້ງໄວ້ກໍ່ຈະເປັຮ False ແທນ 
ໃນ ຕົວດຳເນີນການ Not in ນັ້ນຈະເຮັດວຍກຕົງກັນຂ້າມ  ຫາກບໍ່ພົບເຫັນຈະໄດ້ຜົນລັບປັນ True ແທນ 

1. Operator // in // Object Memberships || Example using: a in b
1. Operator // not in // Negated Object Memberships || Example using: a not in b

'''


names = ['Jo', 'Joe', 'Elite', 'Dy']

if 'Jo' in names:
    print('\'Jo\' exist in the list')

else:    
    print('\'Jo\' not exist in the list')

if 'Phasouk' in names:
    print('\'Phasouk\' exist in the list')
else:
    print('\'Phasouk\' not exist in the list')

    numbers = {'1': 'one', '3': 'three', '2': 'two', '5': 'five'}

if 'one' in numbers.values():
    print('\'one\' exist in the dictionary values')
else:
    print('\'one\' not exist in the dictionary values')

if '7' in numbers.keys():
    print('\'7\' exist in the dictionary keys')
else:
    print('\'7\' not exist in the dictionary keys')

    
