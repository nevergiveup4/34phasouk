'''

List Slicing ຄືສາມາດຕັດຂໍ້ມູນຈາກ List ໜື່ງແລ້ວນຳໄປສ້າງເປັນ List ໃໝ່ໄດ້
ໂດຍວີທີນີ້ເອີ້ນວ່າ Slicing ໃນການຕັດຂໍ້ມູນໃນ List ນັ້ນຈະເຮັດໃນຮູບແບບ [a:b]

'''

# When [a] ເປັນ Index Start ແລະ [b] ເປັນ Index ກ່ອນສະມາຊິກຕົວສຸດທ້າຍທີ່ຕ້ອງການຕັດ ເຊັ່ນ : ຕຳແໜ່ງ Index ທີ່ 5 ຈະບໍ່ສະແດງ ເມືອເຮົາໃຊ້ຄຳສັ່ງກຳນົດ [0:4] ເພາະໄດ້ໃຊ້ Listsiicing ຕັດອອກກ່ອນສະມາຊິກສຸດທ້າຍໜື່ງຕົວ.
# Example : ['a','b','c','d',] // variable name  = variable name[Start:before member finally]

ch = ['a', 'b', 'c', 'd','e', 'f', 'g', 'h', 
    'i','j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 
    'r', 's', 't','u', 'v', 'w', 'x', 'y','z']

a = ch[0:4] # ['a','b','c','d',]
b = ch[4:9]
c = ch[:3]
d = ch[3:]
e = ch[:] #copy all list, or equivalent to e = ch
f = ch[0:2] + ch[6:8] # a - b and g - h

print(a)
print(b)
print(c)
print(d)
print(e)
print(f)



## ການໃຊ້ຄຳສັ່ງ del ເພື່ອລົບຂໍ້ມູນໃນ List ອອກ
'''

ຄຳສັ່ງ del ເປັນຄຳສັ່ງທີ່ໃຊ້ສຳຫຼັບລົບຕົວແປໃດໆ ອອກໄປຈາກໜ່ວຍຄວາມຈຳຫຼືໃຊ້ຍົກເລີກຕົວແປທີ່ເຄີຍປະກາດໄປແລ້ວ ເຮົາສາມາດໃຊ້ຄຳສັ່ງ Del ເພື່ອລົບສະມາຊິກພາຍໃນ List ໄດ້ເຊັ່ນດຽວກັນ

'''

ch = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

del ch[0] # delete 'a'
print(ch)

'''
ໃນພາສາ Python ນັ້ນຕາໃເຂົ້າໃຈແມ່ນ ເມື່ອໃຊ້ del name variable[0] ໄປແລ້ວ ແລະກັບໄປໃຊ້ໃໝ່ອີກເທື່ອ 1 
ໂດຍໃຊ້ຄ່າຂອງ Argument index ຊ້ຳໂຕເກົ່າ Program ກໍຈະທຳການລົບຂໍ້ມູນໃນ List ຕົວຖັດໄປເລີຍ ສາມາດເຮັດ
'''

del ch[0] # delete 'b'
print(ch)


del ch[2:4] # using to del 2 value in list // del name[value1, value2]
print(ch) # delete e, f

del ch [:] #Delete All
print(ch)
