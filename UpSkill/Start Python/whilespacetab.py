
#using to whilespace and tab for true
## ນິຍາມງ່າຍໆເລີຍຄື Whilespace and tab in Python ຫຼັກການເຮັດວຽກຄື ພາຍໃນບ໊ອກຍ່ອຍຈະຕ້ອງມີຫຍໍ້ໜ້າທີ່ສະເໝີກັນຕະຫຼອດຈື່ງສາມາດລັນໄດ້ຫຼືຖືກຕ້ອງຕາມໂປແກຮມ
'''
Whilespace and tab ຄືຄຳສັ່ງບ໊ອກຂອງໂປແກຮມເຊິ່ງໃນ ພາສາ ພາຍຖອນຈະເຂັ້ມງວດເລື່ອງຂອງຈຳນວນຊ່ອງວ່າຕ້ອງເທົ່າກັນເທົ່ານັ້ນ 
ສະເພາະບ໊ອກໃຜລາວເທົ່ານັ້ນຕ້ອງສະເໝີກັນເທົ່າກັນ

 /// Example :

 n = int(input ('Input an integer: '))

if (n > 0):
    print ('x is positive number')
    print ('Show number from 0 to %d' % (n - 1))

else:
    print ('x isn\'t positive number')

for i in range(n):
    print(i)

'''

n = int(input ('Input an integer: '))

if (n > 0):
    print ('x is positive number')
    print ('Show number from 0 to %d' % (n - 1))
else:

    print ('x isn\'t positive number')

for i in range(n):
    print(i)