 ## Using to Dictionary Methods

countries = {'de': 'Germany', 'ua': 'Ukraine',
             'th': 'Thailand', 'nl': 'Netherlands'} 

print(countries.keys()) # Methods Merge Keys all total
print(countries.values()) # Methods Merge Values all total

print(countries.get('de'))
countries.setdefault('tr ', 'Turkey')

print(countries.popitem())
print(countries.popitem())

print(countries.items())


'''

ในตัวอย่าง เป็นโปรแกรมในการใช้งานเมธอดของ Dictionary ตัวแปรของเรา countries มาจากตัวอย่างก่อนหน้าที่มี Key เป็นชื่อย่อของประเทศและ Value เป็นชื่อเต็มของประเทศ
 เมธอด keys() ส่งค่ากลับเป็น List ของ Key ทั้งหมดภายใน Dictionary และเมธอด values() นั้นจะส่งเป็น List ของ Value

หลังจากนั้นเป็นการเข้าถึงข้อมูลด้วยเมธอด
get() โดยมี Key เป็นอาร์กิวเมนต์ซึ่งผลลัพธ์การทำงานของมันจะเหมือนกับการเข้าถึงข้อมูลโดยตรง 
เช่น countries['de'] และเมธอด setdefault() ใช้รับค่าจากคีย์ที่กำหนด ถ้าไม่มีจะเป็นการเพิ่มค่าดังกล่าวเข้าไปใน Dictionary
และต่อมาเมธอด popitem() จะนำสมาชิกตัวสุดท้ายออกจาก Dictionary และส่งค่าดังกล่าวกลับมาเป็น Tuple ออบเจ็ค ส่วนเมธอด items() นั้นจะส่งค่ากลับมาเป็น List ของ Tuple ของออบเจ็คของ Key และ Value ทั้งหมด

'''