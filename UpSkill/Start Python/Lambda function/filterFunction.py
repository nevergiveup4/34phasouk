'''

ຟັງຊັນ filter ໃຊ້ສຳລັບກຮອງຂໍ້ມູນຈາກ Sequence ຈາກຟັງຊັນກຮອງທີ່ກຳນົດເປັນຟັງຊັນ callback ໃນຕົວຢ່າງນີ້ເປັນຟັງຊັນ filter ສຳຫຼັບກຮອງເອົາພຽງເລກຄູ່ພາຍໃນລິດ
ໂດຍການໃຊ້ຮ່ວມກັບ Lamdba function

'''

numbers = [1,2,3,4,5,6,7,8,9.10]

even_numbers = filter(lambda n: n % 2 == 0 , numbers)
print(list(even_numbers))



