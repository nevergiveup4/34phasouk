# Lambda function คืออะไร
# การประกาศ Lambda function
# ตัวอย่างการใช้งาน Lambda function
# การใช้งานกับฟังก์ชัน filter
# การใช้งานกับฟังก์ชัน map

'''

lambda function or Anonymous function is function expression ທີ່ສະແດງເຖິງຄ່າຂອງຟັງຊັນ
ເຊິ່ງສາມາດສ້າງ ແລະ ເກັບຄ່າໄວ້ໃນໄຕົວແປ ຫຼືສົ່ງເປັນພາລາມິເຕີຂອງຟັງຊັນອື່ນໄດ້ ນັ້ນໝາຍຄວາມວ່າມັນບໍ່ຈຳເປັນ
ຕ້ອງມີຊື່ໃນການປະກາດ.

'''

is_even = lambda n : n % 2 == 0  ## ฟังก์ชันนี้รับหนึ่งพารามิเตอร์เป็นตัวเลขที่ต้องการตรวจสอบ จากนั้นส่งค่ากลับโดย Expression ของมันเป็น Boolean เพื่อบ่งบอกว่าตัวเลขเป็นจำนวนคู่หรือไม่ ซึ่งนี่จะมีการทำงานเหมือนกับฟังก์ชันปกติที่ถูกประกาศด้วย

def is_even(n):
    return n % 2 == 0 #ฟังก์ชันทั้งสองรูปแบบให้ผลลัพธ์การทำงานที่เหมือนกัน แต่ Lambda function ถูกออกแบบมาสำหรับการประกาศฟังก์ชันแบบสั้นและกระทัดรัด และสามารถนำไปใช้งานได้ทันที คุณไม่จำเป็นต้องกำหนดมันใส่ในตัวแปรเหมือนกับที่เราทำ

