# using to expression 

'''
expression ຄືການເຮັດວຽກຮ່ວມກັນລະຫວ່າງຕົວແປ 
ກັບຕົວດຳເນີນການໂດຍຄ່າເຫຼົ້ານີ້ຈະມີຕົວດຳເນີນການເປັນຕົວຄວບຄຸມການທຳງານ

ໃນ expression in python ສາມາດແບ່ງ  expression ໄດ້ຢູ່ 2 ແບບຄື: 
1. boolean expression ເປັນການເຮັດວຽກລະຫວ່າງຕົວແປປ ແລະ ຕົວດຳເນີນການປຽບທຽບຄ່າກັນ 
ເຊິ່ງຜົນລັບທີ່ຈະໄດ້ຈະເປັນແບບ Boolean.

2. expression ທາງຄະນິດສາດ ເປັນການເຮັດວຽກຮ່ວມກັນລະຫວ່າງຕົວແປ ກັບ ຕົວດຳເນີນການ
ເຊິ່ງຈະໄດ້ຮັບຄ່າໃໝ່ ເປັນຕົວເລກ ຫຼື ຄ່າຫຍັງກະໄດ້ທີ່ບໍ່ແມ່ນຄ່າ Boolean ນີ້ຈະເອີ້ນວ່າ Expression

'''

#exaample :

a = 4 
b = 5

# Boolean Expression: 

print (a == 4)
print (b == 5)

print(a == 4 and b ==5)
print(a == 4 and b ==8)

#Non- boolean Expressions : 

print (a + b)
print ( a + 2 )
print ( a * b)
print((( a * a) + (b * b)) / 2)
print(" Python " + " Language ")