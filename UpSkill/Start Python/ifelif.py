'''

ຄຳສັ່ງ elif ເປັນຄຳສັ່ງໃຊ້ສ້າງເງື່ອນໄຂ ແບບ ຫຼາຍທາງເລືອກຄືກັບ ການຂຽນ Switch Case ໃນພາສາອື່ນໆ  ແຕ່ elif ນັ້ນຈະໃຊ້ຫຼັງຈາກຄຳສັ່ງ if ສະເໝີ ແລະສາມາດມີ else ໄດ້ໃນເງື່ອນໄຂສຸດທ້າຍ

'''

## Example :

print('Welcome to Laolife\'s game')
level = input('Enter level (1 - 4): ')

if level == '1':
    print('Easy')
elif level == '2':
    print('Medium')
elif level == '3':
    print('Hard')
elif level == '4':
    print('Expert')
else:
    print('Invalid level selected')