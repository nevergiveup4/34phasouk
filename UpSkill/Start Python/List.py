## Using to Lists

'''

List ເປັນປະເພດຂໍ້ມູນທີ່ເກັບຂໍ້ມູນແບບເປັນຊຸດ ແລະ ລຳດັບ ເຊິ່ງມັນສາມາດເກັບຂໍ້ມູນໄດ້ຫຼາຍ
ຄ່າໃນຕົວເເປດຽວ ແລະ ມີ Index ສຳລັບເຂົ້າເຖິງຂໍ້ມູນ ໃນ Python ນັ້ນ List ຈະເປັນຄືກັບ Arryas
ໃນພາສາ C ມັນສາມາດເກັບຂໍ້ມູນໄດ້ຫຼາຍຕົວແລະ ຍັງສາມາດເປັນປະເພດຂໍ້ມູນທີ່ແຕກຕ່າງກັນໄດ້ອີກດ້ວຍ

'''

## Declare lists in python 



# Declare lists
numbers = [1, 2, 4, 6, 8, 19]
names = ["Mateo", "Danny", "James", "Thomas", "Luke"]
mixed = [-2, 5, 84.2, "Mountain", "Python"]

# Display lists
print(numbers)
print(names)
print(mixed)

# Display lists using the for loops

for n in numbers:
    print(n, end ="")

    print()

for n in names:
    print(n, end ="")

    print()

for n in mixed:
    print(n, end ="")

    print()