names = ['Phasouk', 'Jo', 'LOUANGCHALERN', 'Eric']

# ເພີ່ມ 2 ຊື່ຂໍ້ມູນເຂົ້າໄປໃນລິດຂໍ້ມູນ

names.append('ekkanit')
names.append('Jeff Bezos')
print(names)


# ເພີ່ມຊື່ຂໍ້ມູນລົງໄປທີ່ຕຳແໜ່ງທີ່ 3 ຂອງລິດຂໍ້ມູນ

names.insert(2, 'Rich')
print(names)


## ຄຳຟັງລົບຊື່ສະເພາະເຈາະຈົງອອກຈາກ ລິດຂໍ້ມູນ

names.remove('Jo')
names.remove('LOUANGCHALERN')
print(names)

# ການໃຊ້ງານເມຕອດ pop() ໃນລິດ ເປັນການໃຊ້ເມຕອດດືງຂໍ້ມູນສຸດທ້າຍພາຍໃນລິດຂໍ້ມູນອອກມາຈາກຖານຂໍ້ມູນ

print('Popped item = ', names.pop())
print(names)

print('Popped item = ', names.pop())
print(names)


## ການໃຊ້ງານເມຕອດ clear() ຄືລົບຂໍ້ມູນທຸກຢ່າງອອກຈາກລິດລາຍການທັງໝົດ

names.clear()
print(names)
