'''

ໃນ Python Class ຂອງສຕຮິງ ຍັງມີເມຕອດໃນການທຳງານກ່ຽວກັບ text ແລະ ຂໍ້ຄວາມ ເມຕອດເຫຼົ້ານີ້ມັກຈະໃຊ້ສຳຫຼັບການຈັດສະແດງຜົນຂໍ້ຄວາມໃນໂປຮແກຮມຫຼືເວັບໃຫ້ຢູ່ໃນຮູບແບບດຽວກັນ
ເຊັ່ນ: ການປ່ຽນຕົວທຳອິດຂອງປະໂຫຍດໃຫ້ເປັນຕົວພີມໃຫ່ຍ ຫຼື ສະແດງຂໍ້ຄວາມໃນຕົວພີມນ້ອຍຫຼືຕົວພີມໃຫ່ຍ ເປັນຕົ້ນ

'''

s = 'this is LaoLife Enterprise!!'

print(s.capitalize()) # Capitalize() ເປັນຟັງຊັນໃນ Class Str ໃຊ້ໄວ້ປັບປະໂຫຍດຫຼືຂໍ້ຄວາມ ໃຫ້ສະເພາະຕົວໜ້າທຳອິດເທົ່ານັ້ນເປັນພີມໃຫ່ຍເໝືອນການຂຽນຊື່ທາງການຕໍ່ໃຫ້ຕົວຫຼັງທີ່ເຫຼືອຈະເປັນຕົວໃຫ່ຍສຸດທ້າຍກໍຈະຖືກປັບເປັນພິມນ້ອຍທັງໝົດ.

print(s.title()) # Title() ເປັນຟັງຊັນໃນ Class Str ໃຊ້ໄວ້ເຮັດໃຫ້ຕົວແລກໃນທຸກຄຳ ທີ່ຖືກຄັ້ນດ້ວຍຍະວາງ ຫຼື ເອີ້ນວ່າ White space ເປັນຕົວພີມໃຫ່ຍທັງໝົດ

print(s.upper()) # Upper() ເປັນຟັງຊັນໃນ Class Str ໃຊ້ໄວ້ປັບເປັນຕົວພີມໃຫ່ຍທັງໝົດທີ່ຕົວແປປະກາດຄ່າມາ

print(s.lower()) ## lower() ເປັນຟັງຊັນໃນ Class Str ໃຊ້ໄວ້ປັບເປັນຕົວພີມນ້ອຍທັງໝົດທີ່ຕົວແປປະກາດຄ່າມາ

print('Lifetodev.com'.lower())