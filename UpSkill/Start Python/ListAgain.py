'''

List ຄື ໂຄງສ້າງຂໍ້ມູນຊະນິດໜື່ງໃນພາສາ ໄພທອນທີ່ໃຊ້ເກັບຂໍ້ມຸນ ແບບລຳດັບຂອງ (Sequence) 
ໂດຍມີ Index ເປັນຕົວລະບຸຕຳແໜ່ງໃນການເຂົ້າເຖິງຂໍ້ມູນໄດ້

ໃນການປະກາດ List ນັ້ນຂໍ້ມູນຂອງມັນຈະຢູ່ພາຍໃນເຄື່ອງໝາຍ [] ແລະ ຄັ່ນສະມາຊິກແຕ່ລະຕົວດ້ວຍເຄື່ອງໝາຍ ,

'''

numbers = [-1, 2 ,7,8,9, 13]

names = ['Jo', 'Phasouk', 'Eric', 'LOUANGCHALERN' ]
mixed_type = [-2 , 5, 84.2, "BRAIN", "Python"]



numbers = []
numbers.append(-1)
numbers.append(2)
numbers.append(7)
numbers.append(8)
numbers.append(9)
numbers.append(13)

names = ['Jo', 'Phasouk']
names.append('Eric')
names.append('LOUANGCHALERN')


print(numbers)
print(names)


print('numbers count =', len(numbers))
print('names count = ', len(names))