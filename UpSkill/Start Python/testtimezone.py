import time
from datetime import datetime, timezone, timedelta


# Create UTC date objects
utc_date1 = datetime.now(tz=timezone(timedelta(hours = 0)))
utc_date2 = datetime.now(tz=timezone.utc)

print(utc_date1.isoformat())
print(utc_date2.isoformat())

# Time zone in Thailand UTC+7
tz = timezone(timedelta(hours = 7))

# Create a date object with given timezone
date = datetime.now(tz=tz)

# Reading infomation about time zone
print("Time zone offset: %s" % date.utcoffset())
print("Time zone name: %s" % date.tzname())

# Display time
print(date.isoformat(sep = " "))
print(date.ctime())
print(date.utcnow())

tz_offset = int(time.timezone / 3600 * -1)
print("UTC+%d" % tz_offset)
# Output: UTC+7

print(time.timezone/ 3600.0)
tz = timezone(timedelta(hours = 7))

date1 = datetime(1980, 3, 12, 10, 0, 0, tzinfo=tz)
date2 = datetime.fromtimestamp(321678000, tz=tz)
date3 = datetime.fromisoformat("1980-03-12T10:00:00+07:00")

print(date1.isoformat())
print(date2.isoformat())
print(date3.isoformat())

cities = [
    { 'name': 'UTC', 'timezone': 0 },
    { 'name': 'Sydney', 'timezone': 11 },
    { 'name': 'Tokyo', 'timezone': 9 },
    { 'name': 'Bangkok', 'timezone': 7 },
    { 'name': 'London', 'timezone': 0 },
    { 'name': 'Toronto', 'timezone': -5 },
    { 'name': 'Los Angeles', 'timezone': -8 },
]

print("CITY\tLOCAL TIME")

for c in cities:
    tz = timezone(timedelta(hours = c['timezone']))
    date = datetime.now(tz=tz)
    print("%s\t%s" % (c['name'], date.ctime()))