## using to argument (sep) is ຕົວແບ່ງແຍກທີ່ສົ່ງໄປຫຼາຍກວ່າໜື່ງຕົວ ເຊິ່ງມີຄ່າ Default = Whitespace
## using to argument (end) is finally display of function, so default value is \n or new line 

'''
using to function [sep = ', '] is ຈັດລະບຽບຂໍ້ມູນໃຫ້ເປັນລະບຽບໃນການໃຊ້ທີ່ນີ້ຄື ເພີ່ມຈຸດ ໃຫ້ຕົວສະແດງຜົນ
ເພື່ອງ່າຍຕໍ່ການອ່ານ ແລະການຂຽນໂປຮແກຮມໂດຍບໍ່ຈຳເປັນຕ້ອງຈັດຫຼາຍຮອບ
'''

print("Mercury", "Venus", "Earth", sep= ', ')
print("One", end ='')
print("Two", end = '')
print("Three", end = '')


'''
However, print() in python can also send value parameter type of String Formatting 
ໄດ້ໂດຍການໃຊ້ຮູບແບບການແປງຂໍ້ມູນ ທີ່ຄ້າຍກັບການຈັດຮູບແບບການສະແດງຜົນໃນພາສາ C

'''

#EXAMPLE 


lang = "Python"
version = 3.6

print("%s language" %lang)
print("Version %f" %version)
print("%d" %123)
print("%s %f %d" % (lang, version, 123  ))