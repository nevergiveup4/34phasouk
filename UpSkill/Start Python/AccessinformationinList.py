'''

Access Information in List ນັ້ນຈະໃຊ້ Index ສຳຫລັບການເຂົ້າເຖິງຂໍ້ມູນ ໂດຍທີ່ Index ຂອງ List ຈະເປັນຈຳນວນເຕັມທີ່ເລີ່ມຈາກ 0 ແລະ ເພີ່ມຂື້ນທີ່ລະ 1 ໄປເລື່ອຍໆ 
ດັ່ງນັ້ນ ເຮົາຈື່ງສາມາດເຂົ້າເຖິງຂໍ້ມູນພາຍໃນ List ເພື່ອອ່ານຄ່າຫຼືອັບເດດຄ່າໄດ້ ໂດຍຕົງຜ່ານ Index ຂອງມັນ 

'''

#Example : 

names = ['Jo', 'Phasouk', 'Eric ', 'LOUANGCHALERN']
print('names[0] = ', names[0])
print('names[1] = ', names[1])
print('names[2] = ', names[2] )
print('names[-1]', names[-1]) # ຄຳສັ່ງເຂົ້າເຖິງຂໍ້ມູນພາຍໃນ List ເຊິ່ງການໃຊ້ແບບນີ້ຈະໄລ່ແຕ່ Index ສຸດທ້າຍກັບຄືນມາຕາມລຳດັບ


# update value

names[0] = 'Bezos'

print('names[0]', names[0])