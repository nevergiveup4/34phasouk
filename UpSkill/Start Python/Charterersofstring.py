s = 'Mountain'
print(s[0]) # M ຫຼັກການປະກາດຄ່າສະແດງຜົນຮູບແບບ ຂອງ Arrays
print(s[4]) # t
print(s[7]) # n ຫຼັກການປະກາດຄ່າສະແດງຜົນຮູບແບບ ຂອງ Arrays


'''
However ການເຮັດວຽກກັບ String ຜ່ານທາງ Index ນັ້ນສາມາດອ່ານຄ່າໄດ້ພຽງຢ່າງດຽວ ແລະ ບໍ່ສາມາດແກ້ໄຂຄ່າໄດ້
ຈາກຕົວຢ່າງຂ້າງບົນຈື່ງເຮັດໃຫ້ເກີດຂໍ້ຜິດພາດຂື້ນ ເພາະເຮົາພະຍາຍາມປ່ຽນແປງຄ່າຂອງ Stirng ຜ່ານທາງ Index ຂອງມັນ ເຊິ່ງບໍ່ສາມາດເຮັດແບບນັ້ນໄດ້ໃນຮູບແບບນີ້
'''


s = 'Python'
for c in s:
    print(c)

    print()

    s = 'LaoLife.com'
    for i in range(len(s)):
        print('s[%d] = %c' % (i, s[i]))
    print()


'''

ໃນຕົວຢ່າງຕໍ່ໄປນີ້ຈະເປັນການໃຊ້ຄຳສັ່ງຕັດຄຳອອກດ້ວຍ Index  ໃນການໃຊ້ງານນັ້ນໄດ້
ກຳນົດຮູບແບບເປັນ [start:end] ໂດຍທີ່ Start ນັ້ນເປັນຕຳແໜ່ງຂອງ Index ເລີ່ມຕົ້ນທີ່ຕ້ອງການ
ແລະ end ນັ້ນເປັນຕຳແໜ່ງກ່ອນໜ້າຕຳແໜ່ງສຸດທ້າຍທີລະບຸຈຸດສິ້ນສຸດຄ່າທີ່ຕ້ອງການໂດຍອາໄສຫຼັກການນັບ Index

'''

s1 = 'Phasoukk'
print(s1[0:4]) # Moun
print(s1[4:6]) # ta
print(s1[0:1]) # M

s2 = 'Lifetodev'
print(s2[:6]) #marcus
print(s2[6:]) #code


'''
Function len() ທີ່ີມີນຳພາສາ Python ທີ່ໃຊ້ສຳຫລັບຫາຂະໜາດຄວາມຍາວຂອງຕົວອັກສອນ
ຂອງ String ທີ່ລະບຸ ເຊິ່ງຈະໄດ້ຜົນລັບກັບມາເປັນ Integer 
'''


s1 = 'Phasouk'
s2 = 'LOUANGCHALERN'
s3 = 'Python'

print('length of s1 =', len(s1))
print('length of s2 =', len(s2))
print('length of s3 =', len(s3))

