'''

 คุณจะได้เรียนการแปลงประเภทข้อมูลในภาษา Python 
 โดยการใช้ built-in ฟังก์ชัน เนื่องจากในการเขียนโปรแกรมนั้นเรามักจะทำงานกับข้อมูลหลายประเภท 
 ดังนั้น มันจึงมีความจำเป็นที่คุณอาจจะต้องการแปลงข้อมูลประเภทหนึ่งไปเป็นประเภทหนึ่ง เช่น การแปลงตัวเลขให้เป็น String หรือแปลง String เป็นตัวเลข เป็นต้น

'''

## ຟັງຊັນແປງຂໍ້ມູນໃນພາສາ Python


'''

Function	                           Description
int(x [,base])	            แปลงออบเจ็ค x จากฐานที่กำหนด base ให้เป็น Integer
long(x [,base] )	        แปลงออบเจ็ค x จากฐานที่กำหนด base ให้เป็น Long
float(x)	                แปลงออบเจ็ค x ให้เป็น Floating point number
complex(real [,im])	        สร้างตัวเลขจำนวนเชิงซ้อนจากค่า real และค่า imagine
str(x)	                    แปลงออบเจ็ค x ให้เป็น String
repr(x)	                    แปลงออบเจ็ค x ให้เป็น String expression
eval(str)	                ประเมินค่าของ String
tuple(s)	                แปลง Sequence ให้เป็น Tuple ลำดับ (sequence): กลุ่มของค่าที่เรียงไว้ โดยแต่ละค่าถูกระบุได้ด้วยดัชนีที่เป็นจำนวนเต็ม  // รายการ (item): หนึ่งในค่าที่อยู่ในลำดับ // ดัชนี (index): จำนวนเต็มที่ใช้เลือกรายการในลำดับ เช่น อักขระในสายอักขระ ในไพธอน ดัชนีเริ่มต้นที่ 0
list(s)	                    แปลง Sequence ให้เป็น List
set(s)	                    แปลง Sequence ให้เป็น : Tuple  คือ ชุดของข้อมูล ที่จะแสดงตามลำดับ และไม่สามารถเปลี่ยนแปลงข้อมูลในชุดนั้นได้
dict(d)	                    แปลงออบเจ็คให้เป็น Dictionary Dictionary คือ ชุดข้อมูลแบบ unordered สามารถเปลี่ยนแปลงแก้ไข และสามารถอ้างถึง (indexed) ได้ Dictionary ใน Python จะเป็นคู่ key และ value
frozenset(s)	            แปลงออบเจ็คให้เป็น Frozen set
chr(x)	                    แปลงค่าของ Integer ให้เป็น Unicode Char
ord(x)	                    แปลง Charterer ให้เป็นค่า Integer
hex(x)	                    แปลง Integer ให้เป็น Hex string
oct(x)	                    แปลง Integer ให้เป็น Oct string

'''