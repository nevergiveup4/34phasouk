# convert str to int

s = input ('Enter Int number : ')
n = int(s)

print('Type of s is ', type(s)) # ແປງຈາກເລກໄປເປັນຂໍ້ມູນຊະນິດ ສຕຮິງ
print('type of n is ', type(n)) # ແປງຈາກເລກໄປເປັນຂໍ້ມູນຊະນິດ ຈຳນວນເຕັມ
print('Value of n is', n)  # ສະແດງຄ່າທີ່ປ້ອນເຂົ້າ ຈຳນວນເຕັມ

# convert str to float 

s2 = input('Enter floting number : ')
f = float(s2)

print('Type of s2 is ', type(s2)) # ແປງຈາກເລກໄປເປັນຂໍ້ມູນຊະນິດ ສຕຮິງ
print('Type of f is ', type(f)) # ແປງຈາກເລກໄປເປັນຂໍ້ມູນຊະນິດ ຈຳນວນທົດສະນິຍົມ
print('Value of f is ', f) # # ສະແດງຄ່າທີ່ປ້ອນເຂົ້າ ແປງເປັນ ຈຳນວນທົດສະນິຍົມ


'''

คุณยังสามารถแปลงข้อมูลจากประเภทอื่นๆ ให้เป็น String ได้โดยการใช้ฟังก์ชัน str() 
ซึ่งออบเจ็คส่วนมากนั้นสามารถใช้ได้กับฟังก์ชันนี้ ในตัวอย่างของทั้งสามคำสั่ง 
เป็นการแปลงจาก Integer Floating number และ List ไปเป็น String ตามลำดับ

'''

print(str(10+20))
print(str(1.234))
print(str([1,2,3,4,5,6,7])) # Type List