'''
ในภาษา Python มีฟังก์ชันสำหรับทำงานกับ Sequence และ Set โดยปกติแล้วฟังก์ชันเหล่านี้สามารถที่จะแปลงไปกลับได้ 
เช่น การแปลงจาก Tuple ไปยัง List ด้วยฟังก์ชัน list() และหลังจากนั้นทำการแปลงจาก List เป็น Tuple ด้วยฟังก์ชัน tuple() เป็นต้น 
ฟังก์ชัน dict() แปลงข้อมูลแบบ Mapping ให้เป็น Dictionary ส่วนฟังก์ชัน set() นั้นแปลงข้อมูลแบบ Sequence ใดๆ ให้เป็น Set 

มาดูตัวอย่าง

'''

a = list ((1,2,3,4,5,6,7)) # convert tuple to list
print(a)

b = tuple([1,2,3,4,5,6,7]) # convert list to tuple
print(b)

c = set([1,2,3,4,5,6,7]) # convert list to set
print(c)

d = dict(one = 1 , two = 2 , three = 3) # convert mapping to dict
print(d)

e = dict([('one',1), ('two', 2) ,('three', 3)]) # iterable list to dict
print(e)