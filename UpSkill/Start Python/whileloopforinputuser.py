## while loop ຄືຄຳສັ່ງວົນຊ້ຳ ໃຊ້ໄວ້ຄວບຄຸມໂປຮແກຮມເຮັດວຽກບາງຢ່າງຊ້ຳໆ ໃນຂະນະທີ່ເງື່ອນໄຂຍັງເປັນຈິງ
## While loop ງ່າຍໆຄື ຂໍ້ມູນຍັງເປັນຈິງຢູ່ ກໍຈະດຳເນີນຕໍ່ໄປ



number = []
MAX_INPUT = 10

#getting input into list
print('Enter %d numbers to the list' % MAX_INPUT)

i = 1
while i <= MAX_INPUT:
    print('Number %d: ' % i, end = '')
    n =int(input())

    '''

    list.append(obj)  ເປັນເມຕອດຊະນິດໜື່ງທີ່ເພີ່ມວັດຖຸເຂົ້າໄປປຕໍ່ທ້າຍຂອງ ລິດລາຍການນັ້ນ
    ດັ່ງຕົວຢ່າງ : 

    l = [1,2,3,4]
    l.append( 5 )
    print( l )


    Append()

    '''
    number.append(n)
    i += 1

    # displaying numbers from list
print('Your numbers in the list')

sum = 0

i = 1
while i <= MAX_INPUT:
    print(number[i - 1], end = ', ')
    sum += number[i - 1]
    i += 1

print('\nSum = %d' % sum)
print('Average = %.2f' % (sum / MAX_INPUT))
