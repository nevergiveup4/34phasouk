## using to Arithmethic Operators is: 
'''

+ Addition(+) // a + b
+ Subtraction(-) // a - b
+ Multiplication(*) // a * b
+ Division (/)  // a / b
+ Division and floor (//) // a // b
+ Modulo (%) // a % b
+ Power(**) // a ** b

'''

a = 5
b = 3
print("a + b = ", a + b)
print("a - b = ", a - b)
print("a * b = ", a * b)
print("a / b = ", a / b)
print("a // b = ", a // b)
print("a % b = ", a % b)
print("a ** b = ", a ** b)