'''

For Loop ຄືຄຳສັ່ງວົນຊ້ຳທີ່ໃຊ້ຄວບຄຸມການເຮັດວຽກຊ້ຳໆ 
ໃນຈຳນວນຮອບທີ່ແນ່ນອນທີ່ເຮົາກຳນົດໄວ້

range() ຄືຟັງຊັນໜື່ງທີ່ເຮັດໜ້າທີ່ສ້າງລຳດັບຂອງຕົວເລກ ເຊັ່ນ : ລຳດັບຂອງເລກທີ່ຕິດກັນ 0 - 3 
ຫຼືລຳດັບແບບຊ່ວງຫ່າງເທົ່າກັນເຊັ່ນ 2, 4 , 6 , 8 

ເຊິ່ງມີຢູ່ 3 ຮູບແບບຂອງການໃຊ້ງານ : 

1. range(stop) 
2. range(start, stop)
3. range(start,stop,step)

ທັງ start, stop, step ຕ້ອງເປັນເລກຈຳນວນເຕັມເທົ່ານັ້ນ

'''

i = 0
for i in range(5): ## ນີ້ເປັນການໃຊ້ range(stop) ໃນຕົວ ພາຮາມີເຕີ ຈະໃຊ້ເປັນຕົວກຳນົດຫຍຸດການທຳງານໄວ້ພຽງເທົ່ານີ້ 
    print(i)
##
    

for n in range(50, 60): ## ນີ້ເປັນການໃຊ້ range(start, stop) ໃນຕົວ ພາຮາມີເຕີ ຈະໃຊ້ເປັນຕົວກຳນົດເລີ່ມການທຳງານຢູ່ໄສ ແລະ ມີທີ່ຫຍຸດການທຳງານຢູ່ໄສ
    print(n)
##
    
## step ໃນທີ່ນີ້ຄືການ ເພີ່ມຄ່າ ຫຼື ລົດຄ່ານັ້ນເອງຄ້າຍຄືກັບຫຼັກການ i++ , i--
    
    ## for step value is puls
    '''
    
    r[i] = start + step*i when i >= 0 and r[i] < stop
    
    '''
    ## for step value is reduce

    '''
    
    r[i] = start + step*i when i >=0 and r[i] > stop
    
    '''

 
n = int(input('Amount :'))
for j in range(n):
    print('Result = x', end = "")
    print('')


    ### Using range (start,stop, step)

a = list(range(10))
print(a)

b = list(range(1,11))
print(b)

c = list(range(0,30, 5))
print(c)

d = list(range(0, -10, -1))
print(d)

e = list(range(0, 100, +1))
print(e)



