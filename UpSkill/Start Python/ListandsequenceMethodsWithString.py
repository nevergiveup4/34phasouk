'''

ນອກຈາກນີ້ໃນຄລາດ String ຍັງມິເມຕອດທີ່ເຮັດວຽກກ່ຽວກັບ List ໃນການລວມ String ຈາກ List ເຂົ້າດ້ວຍກັນດ້ວຍ Methods join()
ຫຼື ການຕັດຄຳອອກຈາກ Value String ດ້ວຍ Methods Split() Or Methods splitlines()

'''

countries = [ 'England', 'Laos', 'Poland', 'Russia','Germany']

print(','.join(countries))  ## Methods join() all join ຂອງຕົວແປທີ່ປະກາດມາ ລວມ String ເຂົ້າກັນໃນ List ດ້ວຍກັນໂດຍ ຄັ່ນດ້ວຍ , ທີ່ປະກາດ


lang = 'Java PHP C++ Python C#'
## (' ') ນີ້ຄືເອີ້ນວ່າ Argument ຄຶ ຄ່າທີ່ໃສ່ລົງໄປໃນ ຊິງເກີນໂຄດ ນັ້ນແລະເອີ້ນວ່າ Argument ທີ່ສົ່ງໄປ
print(lang.split(' ')) ## Methods lang ນັ້ນຄືເປັນຂໍ້ຄວາມທີ່ຄັ່ນດ້ວຍຊ່ອງວ່າງໃຫ້ ແລະ ເຮົາໄດ້ໃຊ້ Methods split() ເພື່ອແຍກ Stirng Value ອອກຈາກ Variable ແລະຈະໄດ້ຜົນລັບ ເປັນ List


text = 'Python is a language\nused to create a web,\n'
text += 'desktop application\nand more'
text2 = text.splitlines() ## splitlines() ເປັນ Methods ການແບ່ງຄ່າ ຂອງ String ບ່ອນທີ່ມີເຄື່ອງໝາຍ \n ຂື້ນບັນທັດໃໝ່ທຸກຄັ້ງ
for t in text2:
    print(t)