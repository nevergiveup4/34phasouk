countries = {'de': 'Germany', 'ua': 'Ukraine', 'th': 'Thailand', 'nl': 'Netherlands'}

'''

ในลูปแรกเป็นการอ่านค่าแบบ Key และ Value ในแต่ละรอบของการทำงานเราเอาข้อมูลใน Dictionary 
ด้วยเมธอด items() ซึ่งจะส่งค่ากลับเป็น Key และ Value กับมาและโหลดใส่ในตัวแปร k และ v ตามลำดับ

'''
# k = ຄ່າຂອງ Dictionary ຕົວແລກ (Key) ແລະ v = ຄ່າຂອງ Dictionary ຕົວທີ່ 2 (Values)
for k, v in countries.items():

    print(k, v)
    print('Key: ', end= '')

    # ໃຊ້ ForLoop ລວມຄ່າຂອງ Dictionary // Key 
for k in countries.keys():
    print(k, end=' ')
    # ໃຊ້ ForLoop ລວມຄ່າຂອງ Dictionary // Key : Values
print('\nValue:', end = '')
for v in countries.values():    
    print(v, end = ' ')

   



#Python Dictionary Functions : Basic

'''

ฟังก์ชันที่เป็นพื้นฐานและสามารถใช้ได้กับโครงสร้างข้อมูลทุกประเภทคือฟังก์ชัน len() เป็นฟังก์ชันที่ใช้สำหรับนับจำนวนสมาชิกของเจ็ค และ Dictionary ยังมีฟังก์ชัน iter() ที่ทำงานเหมือนกับเมธอด items() นี่เป็นตารางของฟังก์ชันที่สามารถใช้ได้กับ Dictionary

Function	Description
len(dict)	ส่งค่ากลับเป็นจำนวนของออบเจ็คใน Dictionary
iter(dict)	ส่งค่ากลับเป็นออบเจ็คของ Key และ Value

'''
'''

ແລະເຮົາສາມາດໃຊ້ຄຳສັ່ງ Del ເພື່ອລົບຂໍ້ມູນພາຍໃນ Dictionary ອອກໄປໄດ້


'''

 # EXAMPLE :::: 

del countries ['de']




'''


Methods	Description
clear()	ลบข้อมูลทั้งหมดภายใน Dictionary
copy()	คัดลอก Dictionary ทั้งหมดไปยังอันใหม่
get(key[, default])	ส่งค่าข้อมูลใน Dictionary จาก Key ที่กำหนด ถ้าหากไม่มี Key อยู่และไม่ได้กำหนด default จะทำให้เกิดข้อผิดพลาด `KeyError`
items()	ส่งค่ากลับเป็นออบเจ็คของ Key และ Value
keys()	ส่งค่ากลับเป็น List ของ Key ทั้งหมดใน Dictionary
pop(key[, default])	ส่งค่ากลับเป็นค่าสุดท้ายใน Dictionary
popitem()	ส่งค่ากลับเป็น Tuple ออบเจ็คของ Key และ Value
setdefault(key[, default])	ส่งค่ากลับเป็นค่าของ Key ที่กำหนด ถ้าหากไม่มี Key อยู่ใส่ข้อมูลเข้าไปใน Dictionary
update([other])	อัพเดท Dictionary กับคู่ของ Key และ Value จากออบเจ็คอื่น และเขียนทับ Key ที่มีอยู่
values()	ส่งค่ากลับเป็น List ของ Value ทั้งหมดใน Dictionary


'''



