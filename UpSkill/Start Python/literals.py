# using to literals in python
'''
literals ຄື ຂໍ້ມູນທີ່ເປັນຄ່າຄົງທີ່ທີ່ເຮົາໄດ້ປະກາດຂື້ນມາ ຕົວອັກສອນ ເຄື່ອງໝາຍ
 ໃນພາສາພາຍຖອນສາມາດແບ່ງ Data type ໄດ້ໃນແບບຕ່າງໆ ເຊັ່ນ
 integer , floats, nunmber, boolean, string
 ເປັນການປະກາດຄ່າຄົງທີ່ທີ່ບໍ່ມີການປ່ຽນແປງ ແລະ  ຕົວແປ ທີ່ເຮົາປະກາດຂື້ນມາເພື່ອເກັບຂໍ້ມູນທີ່ຕ້ອງການ
'''

# Example :

a = 9 # Data type Integer
b = 22.2 # Data type Float
c = True # Data type Boolean
d = "Hello world" # Data type String


print(type(a))
print(type(b))
print(type(c))
print(type(d))