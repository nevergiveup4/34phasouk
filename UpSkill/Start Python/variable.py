# Variable in python 

'''

Variable in python ຄືຕົວແປຫຼືເຄື່ອງໝາຍທີ່ກຳນົດຂື້ນມາ
ສຳລັບໃຊ້ອ້າງອີງຄ່າທີ່ເກັບຢູ່ໃນໜ່ວຍຄວາມຈຳ

'''

## Example Variable in python

a = 3 # Variable(a) = 3 Data type is Integer
b = 4.92 #type float
c = "MR.PHASOUK LOUANGCHALERN" #Str

print ('a :', a)
print(b)
print(type(c))

