'''

ເມຕອດທີ່ສຳຄັນໃນຄລາດຂອງ String ອີກມຸມໜື່ງຄືເມຕອກໃນການກວດສອບປະເພາດຂອງຂໍ້ມູນພາຍໃນ String
ວ່າເປັນປະເພດທີ່ກຳນົດຫຼືບໍ ເຊັ່ນ : ການກວດສອບຫາວ່າມີຕົວເລກພາຍໃນ String ຫຼືບໍ ຫຼື ກວດສອບວ່າ String ນັ້ນເປັນຕົວເລກ ຫຼືບໍ? 
ດັ່ງເມຕອດຕໍ່ໄປນີ້ ...

'''

# Example tesing Is true.

print('Phasouk2003'.isalnum()) ## isalnum() ເມຕອດນີ້ເປັນເມຕອດໃຊ້ກວດສອບວ່າ ຄ່າທີ່ປະກາດມານັ້ນເປັນ ສະເພາະຕົວອັກສອນ ຫຼື ຕົວເລກທັງໝົດຫຼືບໍ ໂດຍຍຈະບອກແຄ່ວ່າ Ture or False

print('Jo'.isalpha()) ## isalpha()ເມຕອດນີ້ເປັນເມຕອດໃຊ້ກວດສອບວ່າ ຄ່າທີ່ປະກາດມານັ້ນເປັນ ສະເພາະຕົວອັກສອນ a-z ຫຼືບໍ ຖ້າແມ່ນຄືຖືກ ໂດຍບໍ່ມີພວກ ຍະວ່າງ, ເຄື່ອງໝາຍພິເສດ ໄວຍາກອນຕ່າງໆມາກ່ຽວ

print('1234'.isdecimal()) ## isdecimal() ເມຕອດນີ້ເປັນເມຕອດໃຊ້ກວດສອບວ່າ ຄ່າທີ່ປະກາດມາທັງໝົດນັ້ນເປັນເລກ Decimal ຫຼືບໍ ?

print('1234'.isdigit()) ## isdigit() ເມຕອດນີ້ເປັນເມຕອດໃຊ້ກວດສອບວ່າ ຄ່າທີ່ປະກາດມາທັງໝົດນັ້ນເປັນຕົວເລກທັງໝົດຫຼືບໍ ?

print('1234'.isnumeric()) ## isnumeric() ເມຕອດນີ້ເປັນເມຕອດໃຊ້ກວດສອບວ່າ ຄ່າໃນສະຕິງທັງໝົດນັ້ນເປັນຕົວເລກແຕ່ 0-9 ຫຼືບໍ ຖ້າແມ່ນກໍ Ture 

print('phasouk'.islower())

print('PHASOUK'.isupper())

## Example tesing Is False.

print('Phasouk 2003'.isalnum()) # has White space
print('bobby1'.isalpha()) # has number
print('7F'.isdecimal()) # F is not decimal
print('1234A'.isdigit()) # A is not digit
print('Python'.isnumeric()) # not number
print('Mateo'.islower()) # M is upper case
print('Mateo'.isupper()) # Not all are upper case