## using to while loop in python

'''

    while expression: 
        # statements

'''

i = 50

while i >= 0:
    print(i, end = ', ')
    i = i - 5
    print()

    m = 1

    while m <= 10:
        print(2 * m + 1, end = ', ')
        m = m + 1

