'''

Step Using to Function:

'''
# def function_name(args...):
# statements

# def function_name(args...):
# statements
#return value

## Creating Function in python language
def hello(name):
    print('Hello %s' % name)

def count_vowel(str):
    vowel = 0
    for c in str:
        if c in ('A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u'):
            vowel = vowel + 1
    return vowel

def area(width, height):
    c = width * height
    return c

## Calling Function in Python Language
hello('Souksakhone')
hello('LOUANGCHALERN')

print('Vowel in string = %d' % count_vowel('Laolife.com'))
print('Vowel in string = %d' %count_vowel('Python'))
print('Area = %d' %area(8,4))


## Creating Function in python language
def show_info(name, salary = 84360, lang ='Python'):
    print('Name: %s' %name)
    print('Salary: %d' %salary)
    print('Language : %s' %lang)
    print()


## Calling Function in Python Language
show_info('Phasouk')
show_info('Phasouk', 150000)
show_info('Jo', 180000, 'JAVA')



def create_button(id, color = '#ffffff' , text = 'Button' , size = 16):
    print('Button ID: %d' %id)
    print('Attributes:')
    print('Color: %s' %color)
    print('Text: %s' %text)
    print('Size: %d px' %size)
    print()

#Calling Function
create_button(10)
create_button(11, color = '#4286f4', text= 'Sign up')
create_button(id = 12, color = '#323f54', size = 24)
create_button(color = '#1cb22b' , text= 'Log in' , size=32, id =  13)


print(1, 2, 3)
print(1, 2, 3, sep = '-', end = '/')


## Lambda Expressions Function

'''
Lambda Expressions Function ຄື anonymous function ທີ່ເປັັນຟັງຊັນທີ່ມີການທຳງານຂະໜາດນ້ອຍຢູ່ພາຍໃນ
ທີ່ສາມາດມີໄດ້ພຽງ Expression ດຽວເທົ່ານັ້ນ , ເຊິ່ງ Expression ຄື ເປັນການກະທຳກັນລະຫວ່າງຕົວແປ ແລະ ຕົວດຳເນີນການຄະນິດສາດ
ແລະຈະໄດ້ຮັບຄ່າໃມ່ເປັນຕົວເລກທີ່ບໍ່ແມ່ນຄ່າ Boolean
'''

f = lambda x: x + 1
print(f(2))
print(f(8))

g = lambda a, b: (a + b) / 2
print(g(3, 5))
print(g(10, 33))

def make_incrementor(n):
    return lambda x: x + n

f = make_incrementor(13)
print(f(0))
print(f(1))
print(f(5))



numbers = [2, 15, 5, 7, 10, 3, 28, 30]
print(list(filter(lambda x: x % 5 == 0, numbers)))
print(list(map(lambda x: x * 2, numbers)))