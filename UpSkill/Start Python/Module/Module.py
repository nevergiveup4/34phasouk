'''
Module Python ຄືໄຟລຂອງໂປແກຮມທີ່ກຳນົດຕົວແປ ຟັງຊັນ ຫຼື ຄາດໂດຍແບ່ງຍ່ອຍອອກໄປຈາກໂປຮແກຮມຫຼັກທີ່ເຮົາສາມາດດືງມາໃຊ້ໄດ້ຢ່າງງ່າຍດາຍ
ແລະສາມາດນຳມາໃຊ້ໄດ້ໂດຍການນຳເຂົ້າມາໃນໂປຮແກຮມ (Import) ກ່າວອີກແບບໜື່ງກໍຄື : 
ໂມດູນກໍຄື : ໄລບຮາລີທີ່ສ້າງົວ້ແລະນຳມາໃຊ້ງງານໃນໂປຮແກຮມນັ້ນລະ

What is Module ?
Create Module ? 
Using to Module ? 
Package type Name Space ? 


'''

# What is Module ?

'''

โมดูล (Module) คือไฟล์หรือส่วนของโปรแกรมที่ใช้สำหรับกำหนดตัวแปร ฟังก์ชัน หรือคลาสโดยแบ่งย่อยอีกหน่วยหนึ่งจากโปรแกรมหลัก 
และในโมดูลยังสามารถประกอบไปด้วยคำสั่งประมวลผลการทำงานได้ ยกตัวอย่างเช่น เมื่อคุณเขียนโปรแกรม 
ในภาษา Python คุณอาจจะมีฟังก์ชันสำหรับทำงานและจัดการกับตัวเลขเป็นจำนวนมาก และในขณะเดียวกัน คุณไม่ต้องการให้โปรแกรมหลักนั้นมีขนาดใหญ่เกินไป 
นั่นหมายความว่าคุณสามารถนำฟังก์ชันเหล่านี้มาสร้างเป็นโมดูล และในการใช้งานนั้นจะต้องนำเข้ามาในโปรแกรมโดยวิธีที่เรียกว่า Import



คุณจะเห็นว่าโมดูลก็คือการแยกส่วนของโปรแกรมออกไปเป็นอีกส่วนและสามารถเรียกใช้ได้เมื่อต้องการ หรือกล่าวอีกนัยหนึ่ง โมดูลก็เหมือนไลบรารีของฟังก์ชันและคลาสต่างๆ 
นั่นเป็นเพราะว่าเมื่อโปรแกรมของคุณมีขนาดใหญ่ คุณสามารถแบ่งส่วนต่างๆ ของโปรแกรมออกเป็นโมดูลย่อยๆ เพื่อให้ง่ายต่อการจัดการและการใช้งาน 
ในภาษา Python โมดูลที่ถูกสร้างขึ้นมานั้นจะเป็นไฟล์ในรูปแบบ module_name.py และนอกจากนี้ Python ยังมี Built-in module เป็นจำนวนมาก
 เช่น math เป็นโมดูลเกี่ยวกับฟังก์ชันทางคณิตศาสตร์ หรือ random เป็นโมดูลเพื่อจัดการและสุ่มตัวเลข เป็นต้น

'''

# Create Module in Python
