# number.py module
# ສ້າງຟັງຊັນສຳລັບຫາຄ່າ ຈຳນວນ ເຕັມຂອງ n
def factorial(n): # return factorial value of n
    if n == 0 or n == 1:
        return 1
    else:
        return n * factorial(n - 1) # n * ຄ່າຂອງ factorial ແລ້ວ Argument ຄ່າລົບເທື່ອລະ 1
    
def fibonacci(n): # return fibonacci series up to 
    result = []
    a, b = 0, 1
    while b < n :
        result.append(b) # append Python คือคำสั่งสำหรับเพิ่มข้อมูลลงในตัวแปร List
        a, b = b, a + b
        return result
    
'''

ในตัวอย่าง เป็นการสร้างโมดูลโดยไฟล์ของโมดูลนั้นมีชื่อว่า number.py นั่นหมายความว่าโมดูลนี้มีชื่อว่า number ซึ่งนี่เป็นสิ่งที่เราจะใช้สำหรับเรียกใช้งานโมดูลในการเขียนโปรแกรม ภายในโมดูลประกอบไปด้วย 2 ฟังก์ชันที่ทำงานเกี่ยวกับตัวเลข 
ฟังก์ชัน factorial() เป็นฟังก์ชันสำหรับหาค่า Factorial ของตัวเลขจำนวนเต็ม n ซึ่งเป็น Recursive function และฟังก์ชัน fibonacci() ใช้หาลำดับของ Fibonacci จนถึงจำนวนเต็ม n

'''


    