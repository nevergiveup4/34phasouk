

image/                          Top-level package
      __init__.py               Initialize the image package
      formats/                  Subpackage for file format
              __init__.py
              jpeg.py
              gif.py
              png.py
              ...
      filters/                  Subpackage for image filters
              __init__.py
              blur.py
              noise.py
              render.py
              ...
      edit/                  Subpackage for editing images
              __init__.py
              crop.py
              grayscale.py
              invert.py
              resize.py
              ...

__all__ = ["jpeg", "gif", "png"]