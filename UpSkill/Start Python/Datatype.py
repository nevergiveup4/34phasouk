#Data Type in Python

'''
Data type ຄືໍຊະນິດຂອງຂໍ້ມູນທີ່ປະກາດຂື້ນມາ ສາມາດແຍກອອກເປັນ 2 ປະເພດຄື :
1. ຊະນິດຂໍ້ມູນພື້ນຖານ (Primitive Data type) ໝາຍເຖິງຊະນິດຂໍ້ມູນທີ່ສາມາດເກັບຂໍ້ມູນທີ່ເປັນຂໍ້ມູນແບບທົ່ວໄປໄດ້
ຫຼືຂໍ້ມູນພື້ນຖານນັ້ນເອງ  ຕົວຢ່າງຂອງປະເພດຕົວແປໃນພາຍຖອນ :

- String ຈັດເກັບຕົວອັກສອນ // Example : a = "Phasouk"
- Boolean ຈັດເກັບຄ່າຈິງຫຼືປອມ // True and false
- Float ຈັດເກັບຄ່າທີມີທົດສະນິຍົມ // 1.00 1.222 -0.1111 etc.
- Integer ຈັດເກັບຄ່າຈຳນວນເຕັມ // 2000 0 -1
- List ຈັດເກັບຂໍ້ມູນແບບ ລິສ // [1, 0, -555]
- Dictionary ຈັດເກັບຂໍ້ມູນແບບ Dictionary // [{'a': '1112', 'b': 0, 'c':555}]


2. ຊະນິດແບບຂໍ້ມູນອ້າງອີງ Reference Data type ມີຄວາມແຕກຕ່າງກັບຊະນິດຂໍ້ມູນພື້ນຖານທີ່ວ່າຊະນິດຂໍ້ມູນ
ຊະນິດນີ້ຢູ່ໃນຮູບແບບໜື່ງ ເຊິ່ງການເຂົ້າເຖິງ ໃຊ້ງານ ຂໍ້ມູນເປັນການອ້າງເຖິງຂໍ້ມູນຈື່ງສາມາດເຂົ້າເຖິງຂໍ້ມູນສ່ວນນັ້ນໄດ້ ຕົວຢ່າງຂອງປະເພດຕົວແປໃນພາຍຖອນ :

- Class 
- Object
- Array

ເຊິ່ງວິທີການເກັບຊໍ້ມູນ ເນື່ອງຈາກ ພາສາ Python ຈະທຳການເລືອກປະເພດຂໍ້ມູນໃຫ້ອັດຕະໂນມັດຫາກ ຜູ້ໃຊ້ງານບໍ່ໄດ້ທຳການລະບຸກ່ອນ ເຮົາຈື່ງສາມາດເກັບຂໍ້ມູນເຂົ້າໄປໃນຕົວແປໄດ້ເລີຍດັ່ງນີ້
/// Example : 

name = "Jeff Bessus"
age = 22
height = 123.45
sus_flag = False
tag = ['Amogus', 'Red', 'Reporter']

ເຊີ່ງວ່າຕົວແປເຫຼົ້ານີ້ name,age,heoght,susflag,tag ກໍຈະມີການເກັບຂໍ້ມູນເປັນປະເພດ String , Integer, float, Boolean , List ຕາມລຳດັບທີ່ຂຽນໂຄ້ດໄວ້

''' 
## Data Type Numbers : 
'''
    ໃນພາສາ Python ນັ້ນຈະສະໜັບສະໜູນຂໍ້ມູນແບບຕົວເລກ ເຊິ່ງຂໍ້ມູນຈະເເບ່ງອອກເປັນ Integer , Float, Decimal and Complex ເຊິ່ງສຳລັບເລກແບບ Decimal ໃນທີນີ້ຈະແຕກຕ່າງໄປຈາກການໃຊ້ Float 
    ຄືສາມາດເກັບຄວາມລະອຽດຂອງຈຳນວນທົດສະນິຍົມໄດ້ຫຼາຍກວ່າ Float ນັ້ນເອງ

    ## ຕົວເລກໃນຮູບແແບບ Complex ຈຳນວນສະເພາະ
    '''

## Integer in Python 

a = 7
b = 3
c = a + b
d = a / b

print ('a = %d' % a)
print ('b = %d' % b)
print ('c = %d' % c)
print ('d = ', d)


## Floating point number
## ການເອີ້ນໃຊ້ Method ຄ້າຍຄືກັບການຂຽນ ພາສາ C
speed = 34.12
pi = 22 / 7
height = 2.31E5
length = 1.3E-3

print ('speed = %f' % speed)
print ('pi = %f' % pi)
print ('height = %f' % height)
print ('length = %f' % length)
print (pi)


### Strings in python 
'''

ໃນພາສາ Python ການໃຊ້ງານ Strings 
ເປັນຕົວອັກສອນຫຼາຍຕົວລຽງກັນ ແລະ ຈະຢູ່ໃນເຄື່ອງ Double quote and single quote

'''

## EXAMPLE IN PYTHON 

name = "Phaasouk"
country = "Laos"
language = 'Python'
interest = ' Mountain Everest'

print(name)
print(country)
print(language)
print(interest)

sentent1 = "What's your name?"
sentent2 = 'I\'m Mateo.'
sentent3 = "He said \"I would learn Python first\"."
sentent4 = 'His teach replied "Oh well!"'
print(sentent1)
print(sentent2)
print(sentent3)
print(sentent4)



site = 'Jo'

tutorial = 'Python'

print(site)
print(tutorial)




