    ## using to Comparison operators is :

'''

< less than // a < b
<= Less than or equal // a <= b
> Greater than // a > b
>= Greater than // a >= b 
== Equal  // a == b
!= Not equal // a != b
is Object identity // a is b 
is not Negated object identity // a is not b

'''

## Constant comparison 

print('4 == 4 : ', 4 == 4)
print('1 < 2 : ', 1 < 2)
print('3 > 10 : ', 3 > 10)
print('2 <= 1.5', 2 <= 1.5)
print()


# Variable Comparison
a = 10 
b = 8
print('a != b : ', a != b)
print('a - b == 2 : ', a - b == 2)



## Example OOP in python :

class Person:

    def __init__(self, name):
        self.name = name

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

p1 = Person('Tommy')
p2 = Person('Jane')
# Tell p3 to use memory address of p1
p3 = p1

print('Memory address of a: ', id(p1))
print('Memory address of b: ', id(p2))
print('Memory address of c: ', id(p3))

print('p1 name:', p1.getName())
print('p3 name:', p1.getName())

print('Changed p3 name to \'Tom\'')
p3.setName('Tom')
print('p1 name:', p1.getName())
print('p3 name:', p3.getName())

print('p1 is p2:', p1 is p2)
print('p1 is p3:', p1 is p3)

print('Changed p3 name to \'Tom\'')
p3.setName('Tom')


## Using to If-else if operator 

n = int(input("Enter a number: "))

if n % 2 == 0:
    print('%d  is even number' % n )
else:
    print('%d is odd number' % n)
    
if n > 0:
    print('%d is positive nunber' % n )

elif n == 0:
    print('%d is zero' % n)

else:
    print('%d is negative number' %n)



    ## using to if else if to create website in python:

print('Log in page')
username = input('username: ')
passoword = input('password: ')

if (username == 'Phasouk' and passoword == '02022003'):
    print('Welcom Phasouk, you\'ve loged in.')
else:
    print('Invalid username or assword.') 