'''

ການໃຊ້ງານເມຕອດຂອງ List : 

'''

names = ['Jo','Top','Toon','Ton','Bibie','EQ','Rich']

# add 2 names to the end of the list 
# // ເພີ່ມຂໍ້ມູນເຂົ້າມາຕື່ມໃນສ່ວນທ້າຍຂອງ List ໂດຍໃຊ້ append()

names.append('Phasouk')
names.append('LOUANGCHALERN')
print(names)

##add a name at position 3 
#ເພີ່ມຊື່ຂໍ້ມູນລົງໄປໃນຕຳແໜ່ງທີ່ 3 ຂອງ List 

names.insert(2, 'Eric') # ຊື່ຕົວແປ.ອິນເຊີນ(ຕຳແໜ່ງທີ່ຕ້ອງການເພີ່ມຂໍ້ມູນລົງ , ຂໍ້ມູນທີ່ຕ້ອງການເພີ່ມລົງ)
print(names)

# using to remove 2 specific names 
## ຄຳສັ່ງໃຊ້ໄວ້ລົບ 2 ຊື່ສະເພາະອອກຈາກ List ໂດຍໃຊ້ຄຳສັ່ງ

'''

names.remove('Toon') # ລົບຊື່ຕຸ້ນອອກຈາກຖານຂໍ້ມູນຂອງ List
names.remove('Top') # ລົບຊື່ທັອບອອກຈາກຖານຂໍ້ມູນຂອງ List

'''



# Using to List with Methods

fruits = ['banana', 'orange', 'apple', 'grape', 'apple', 'peach']

print(fruits)

'''

fruits.count(ຊື່ຕົວແປທີ່ຕ້ອງການຈະສົ່ງຄ່າ) ເປັນເມຕອດທີ່ໃຊ້ສຳລັບນັບຈຳນວນອັອບເຈັກວ່າມີຢູ່ເທົ່າໃດ ທີ່ຕົງກັນກັບ
ທີ່ລະບຸພາຍໃນ List ເຊິ່ງໃນຕົວຢ່າງລຸ່ມນີ້ເຮົາໄດ້ທຳການນັບ 2 ອັອປເຈັກທີ່ໄດ້ຈັດເກັບລົງໄປໃນ List ແລະ ເອີ້ນໃຊ້ Count() ເພື່ອນັບຈຳນວນທີ່ມີຢູ່ຂອງອັອບເຈັກ 

'''

print ('%d apples in list' % fruits.count('apple'))
print('%d banana in list' % fruits.count('banana'))

'''

index() นั้นใช้สำหรับหาตำแหน่ง Index ของออบเจ็คตัวแรกที่พบใน List 
เราได้ใช้เมธอดเพื่อหา Index ของ 'grape' และแสดงผลตำแหน่งที่ได้ , ຖ້າບໍ່ພົບກໍຈະເກີດເອີເລີທັນທີ່

'''
print('Index of grape is %d' % fruits.index('grape'))

'''

fruits.sort() ເປັນການໃຊ້ເມຕອດສຳລັບການລຽງລຳດັບຂໍ້ມູນພາຍໃນ ຕົວແປ List ທີ່ມີຢູ່ຈາກເລີ່ມຕົ້ນໄປຫຼັງ ໃນກໍລະນີທີ່ List
ເປັນຕົວເລກກໍ່ເຊັ່ນກັນຈະລຽງຈະນ້ອຍໄປຫຼາຍ 

 reverse() ใช้สำหรับย้อนกลับข้อมูลภายใน List จากตำแหน่งแรกไปตำแหน่งสุดท้ายและในทางกลับกัน


'''
fruits.sort()
print('Sorted', fruits)
fruits.reverse()
print('Resersed', fruits)

'''
append(x)	 เพิ่มออบเจ็ค x เข้ามายัง List
extend(iterable)	เพิ่มข้อมูลจาก iterable เข้ามายัง List
insert(i, x)	ใส่ออบเจ็ค x เข้ามายัง List ที่ตำแหน่ง i
remove(x)	นำออบเจ็ค x อันแรกที่พบใน List ออก
pop([i])	นำข้อมูลอันสุดท้ายออกจาก List
clear()  	ลบข้อมูลทั้งหมดภายใน List
index(x[, start[, end]])	ค้นหาตำแหน่งของออบเจ็ค x
count(x)	นับออบเจ็ค x ใน List
sort(key=None, reverse=False)	เรียงข้อมูลภายใน List จากน้อยไปมาก
reverse()	  ย้อนกลับลำดับของ List
copy()	คัดลอก List ทั้งหมด

'''



