
import sys 

a = 8 
b = 13.4
c = "Python"
d = [1, 2, 3, 4]

print('Size of a = ', sys.getsizeof(a))
print('Type of a = ', type(a))


print('Size of b = ', sys.getsizeof(b))
print('Type of b = ', type(b))

print('Size of c = ', sys.getsizeof(c))
print('Type of c = ', type(c))

print('Size of d = ', sys.getsizeof(d))
print('Type of d = ', type(d))


del a  ## del is command for delete variable other from memory or cancel variable used it in the past (ທີ່ເຄີຍໃຊ້ຜ່ານມາ)
del b, c, d

if 'a' in locals():
    print("a is exist")
else:
    print("a is not exist")