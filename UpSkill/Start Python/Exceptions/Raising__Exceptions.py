'''

ในภาษา Python มี build-exception ที่จะเกิดขึ้นโดยพื้นฐานเมื่อโปรแกรมมีข้อผิดพลาดขึ้น อย่างไรก็ตามโปรแกรมเมอร์สามารถสั่งให้เกิด
 Exception ขึ้นเองได้ โดยการใช้คำสั่ง raise มาดูตัวอย่างการใช้งาน

'''

try:
    name = input('Enter your name: ')
    if name == 'Phasouk':
        raise Exception('Whoa! Phasouk you are not allowed here')
    print('Hi', name)

except Exception as err:
    print("Exception: ", err)
    
else: 
    print("Bye")