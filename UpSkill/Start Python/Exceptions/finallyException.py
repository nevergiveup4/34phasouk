'''

อีกคำสั่งหนึ่งที่จำเป็นสำหรับการจัดการข้อผิดพลาดก็คือคำสั่ง finally ที่สามารถใช้ร่วมกับคำสั่ง try ... except 
ได้ โดยการทำงานของมันนั้นจะแตกต่างจาก else คือจะทำงานในบล็อคคำสั่งนี้เสมอ ไม่ว่าจะเกิดข้อผิดพลาดหรือไม่ก็ตาม

'''

try: 
    items = ['Mac','iPhone', 'iPad']

    print('Avilable items: ', items)
    need = input('What do you want to buy?: ')
    if  need not in items:
        raise Exception('Sorry, \'' + need + '\'' + ' out of stock')
    print('You have purchased '+ '\'' + need + '\'')

except Exception as e:
    print(e)

finally:
    print("Thank you for  shopping with us")