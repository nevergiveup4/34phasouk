'''

ນອກຈາກການໃຊ້ງານ build-in exception ຈາກພາສາ Python ແລ້ວ ເຈົ້າຍັງສາມາດສ້າງຄລາດ Exception ຂື້ນມາເອງໄດ້
ເພື່ອໃຫ້ສາມາດເຮັດວຽກໄດ້ຕາມທີ່ຕ້ອງການ ເຊັ່ນ : ການເພີ່ມແອັດທິບິວຫຼືເມຕອດຕ່າງໆ ພາຍໃນຄລາດ
ຕໍ່ໄປເຮົາຈະມາສ້າງຄລາດເພື່ອຈັດການຂໍ້ຜິດພາດຂອງເຮົາເອງ ໂດຍໃນການສ້າງຄລາດ ນັ້ນເຮົາຕ້ອງທຳການສືບທອດມາຈາກຄລາດ Exceptions 

'''

# ການສ້າງຄລາດເຊິ່ງເຈົ້າອາດຈະສ້າງໄວ່ທີ່ໂມດູນອື່ນເພື່ອເອີ້ນໃຊ້ງານ
 
class UsernameError(Exception):

    def __init__(self, message, error):
        super().__init__(message)
        self.message = message
        self.error = error

    def getMessage(self):
        return self.message + ' \'' + self.error + '\''
    
class PasswordError(Exception):

    def __init__(self, massage, error):
        super().__init__(massage)
        self.message = message
        self.error = error

    def getMessage(self):
        return self.message + '\'' + ('*' * len(self.error)) + '\''
    
# ໂປຮແກຮມເລີ່ມການທຳງານ 
try: 
    print('Login')
    username = input('Username: ')
    password = input('Password: ')

    if (username != 'Phasouk'):
        raise UsernameError('Invalid username', username)
    
    if (password != '1234'):
        raise PasswordError('Invalid password', password)
    print('Login succes')


except UsernameError as e:
    print('Exception: ', e.getMessage())

except PasswordError as e:
    print('Exception: ', e.getMessage())

   

'''

ในตัวอย่างของโปรแกรมนั้นจะแบ่งออกเป็นสองส่วน ในส่วนแรกเป็นการสร้างคลาสโดยเราได้สร้างคลาสมาสองคลาสคือ UsernameError เป็นคลาสของ Exception สำหรับจัดการเมื่อ username ไม่ถูกต้อง 
และคลาส PasswordError เป็นคลาสของ Exception สำหรับจัดการข้อผิดพลาดเมื่อรหัสผ่านไม่ถูกต้อง โดยในคลาสเราได้กำหนดแอตทริบิวต์สองตัวคือ message เป็นความสำหรับอธิบายข้อผิดพลาด 
และ error เป็นข้อมูลที่เกิดข้อผิดพลาดขึ้น และภายในคลาสทั้งสองมีเมธอด getMesssage() สำหรับรับข้อความการแสดงข้อผิดพลาดที่แตกต่างกันออกไป

ในส่วนต่อมา เป็นการทดสอบการจัดการข้อผิดพลาดของเรา โดยการจำลองการทำงานระบบ Login สำหรับให้ผู้ใช้เข้าสู่ระบบโดยการใส่ username และ password 
โดยเราจะทำการตรวจสอบถ้าหากชื่อผู้ใช้ไม่เป็น "mateo" เราจะทำให้เกิดข้อผิดพลาด UsernameError ขึ้น แต่ถ้าชื่อผู้ใช้ถูกต้องแต่รหัสผ่านยังผิดจะทำให้เกิดข้อผิดพลาด PasswordError ขึ้น
 นอกจากนี้ หมายความว่าการเข้าสู่ระบบสำเร็จ

'''
    

'''

ນີ້ຄືຂະບວນການສ້າງຄວາມປອດໄຟໃນການສ້າງບັນຊີລ໊ອກອິນເຂົ້າໃຊ້
ເບື້ອງຕົ້ນຂອງລະບົບປ້ອງກັນຢ່າງໃດຢ່າງນື່ງໃຊ້ຂະບວນການນີ້ Class , Object ເຊັ່ນ :

ເມື່ອມີການກຮອກ ຊື່ຜູ້ໃຊ້ແລະລະຫັດຜ່ານຖືກຕ້ອງ ໂປຮແກຮມກໍຈະເດັ້ງໄປສູ້ໜ້າຫຼັກຂອງໂປຮແກຮມໄດ້

'''

