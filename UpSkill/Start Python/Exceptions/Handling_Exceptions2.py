import sys

try:
    f = open('file.txt')
    s = f.readline()
    print(s)

except OSError as err:
    print("OS error: " , err)

except:
    print("Unexpectef error occured")

else:
    print("file closed successfully")
    f.close()