'''

In Python, Class of String is also method in ການຈັດຮູບແບບຂອງ String ສຳຫຼັບການສະແດງຜົນອີກດ້ວຍ ເຊັ່ນ ການເຕີມຕົວອັກສອນເຂົ້າໄປໃນຕົວ String or ການລົບຕົວອັກສອນທີ່ບໍ່ຕ້ອງການອອກໄປຈາກ String

'''

s ='Phasouk'

print(s.center(20)) # center() ໃນເມຕອດຂອງ Str ໃຊ້ສຳຫຼັບເຕີມຂໍ້ຄວາມເຂົ້າໄປທາງຊ້າຍ ແລະ ທາງຂວາໃຫ້ Str ມີຄວາມຍາວກັບທີ່ລະບຸ ເອົາງ່າຍໆຍັບຕົວແປທີ່ສ້າງຂື້ນໄປຢູ່ຕົງກາງຈົບ

print(s.ljust(20)) # ljust() ໃນເມຕອດຂອງ Str ໃຊ້ສຳຫຼັບເຕີມອັກສອນສະເພາະດ້ານທາງຊ້າຍ ເອົາງ່າຍໆຍັບຕົວແປທີ່ສ້າງຂື້ນໄປຢູ່ຊ້າຍສຸດ

print(s.rjust(20)) # ljust() ໃນເມຕອດຂອງ Str ໃຊ້ສຳຫຼັບເຕີມອັກສອນສະເພາະດ້ານທາງຂວາ ເອົາງ່າຍໆຍັບຕົວແປທີ່ສ້າງຂື້ນໄປຢູ່ຂວາສຸດ

# Example using to Formatting With String in Python.

print(s.center(10,'!'))
print(s.ljust(10, '!'))
print(s.rjust(10, '!'))


number = '83'

print(number.zfill(6)) ## zfill() ເປັນຟັງຊັນການເຕີມຄ່າ Default = 0 ໃຫ້ຈຳນວນທີລະບຸໃນ Parameter() ແລ້ວເອົາໄປລວມກັບຈຳນວນຕົວແປທີ່ປະກາດກ່ອນໜ້ານຳຈື່ງອອກມາເປັນຄຳຕອບ
print('-1.32'.zfill(8)) # result = zfill() + variable value // zfill() + variable value = result by fill 0 ເຂົ້າໄປຈຳນວນທີ່ຂາດ


'''
'''

s = '   Phasouk '
print(s.strip()) # strip() ເປັນຟັງຊັນໃນຄລາດ String ໃຊ້ໄວ້ຕັດລຳດັບຂອງຕົວອັກທີ່ພົບທັງເບື້ອງຊ້າຍແລະເບື້ອງຂວາອອກຈາກຂໍ້ຄວາມ ເຊິ່ງໃນນີ້ເຫັນວ່າຈະເປັນ While Space ເຮົາກໍທຳການຕັັດອອກທັງໝົດທັງໜ້າແລະຫຼັງບໍ່ມີຍະວາງລົງເຫຼືອ

print(s.lstrip()) ## ຕັດສະເພາະທາງຊ້າຍ
print(s.rstrip()) # ຕັດສະເພາະທາງຂວາ


# Example using to Formatting With String in Python.

s2 = '----LaoLife----'
print(s2.strip('-'))
print(s2.lstrip('-'))
print(s2.rstrip('-'))

# Python also format() methods ສຳຫຼັບຈັດຮູບແບບການສະແດງຜົນຂອງ String ທີ່ເອີ້ນວ່າ Stirng Interpolation ການເຮັດວຽກຂອງເມຕອດນັ້ນຈະຄ້າຍຄືກັບການຈັດການສະແດງງຜົນໃນຟັງຊັນ print()

'''

Argument ຄືຄ່າທີ່ຈະສົ່ງອອກໄປ ໃຫ້ເມຕອດ ເຊັ່ນ {0} is Argument Value

'''
print('a = {0}'.format(3))
print('{0} loves to learn {1}'.format('Phasouk', 'Python'))